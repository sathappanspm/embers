#!/usr/bin/env python
#-*- coding:utf-8 -*-
# vim: ts=4 sts=4 sw=4 tw=79 sta et
"""
    *.py: Description of what * does.
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = "chainer"
from utils import isodate, Similarity, maxHeap, initializeIDF, updateDictCount
from dbHandler import columnFamily, dbConnect
import uuid
from math import log as logarithm
from etool import args, logs, queue
from datetime import datetime
import copy

log = logs.getLogger('%s-%s.log' % (__processor__, str(datetime.now())))
minChainThreshold = 0.8
minLastEleChainThreshold = 0.8
CHAIN_NUM = 0


class Chainer(object):
    def __init__(self, CONFIG_DIR, dbname,  dbaddr, buffersize,
                 distanceMeasure='cosine'):
        self.dbCon = dbConnect(dbname, dbaddr)
        self.articleDb = columnFamily(self.dbCon, 'Articles', buffersize, ['publishedDate'])
        self.chainDb = columnFamily(self.dbCon, 'Chains', 1, ['lastUpdateTime'])
        initializeIDF(CONFIG_DIR)
        self.dirty_list = False
        if distanceMeasure == 'cosine':
            self.calculateDistance = lambda x, y: 1 - Similarity.cosineSimilarity(x, y)
            log.info('Using Cosine Distance')
        elif distanceMeasure == 'soergels':
            self.calculateDistance = Similarity.soergelsDistance
            log.info('using Soergels Distance')
        return

    def close(self):
        self.articleDb.flush()
        self.chainDb.flush()
        self.dbCon.close()

    def chain(self, article):
        """Obtain last n days articles of the given country and pick the top
           5 most articles above a given threshold and chain"""
        if 'None' in article['embersId']:
            print "Wrong Id"
            exit(0)
        article['publishedDate'] = isodate(article['publishedDate'])
        article['chainIds'] = []
        for cKey, cProp in self.linkToChain(article):
            for eleKey, eleProp in self.articleDb.getMany(cProp['elements']):
                eleProp['chainIds'].append(cKey)
                self.insertArticle(eleProp, eleKey)
            self.insertChain(cKey, cProp, article)
            article['chainIds'].append(cKey)
        self.insertArticle(article, article['embersId'])
        return True

    def getCombinedDistance(self, doc1, doc2, cosineDistance, threshold):
        """ returns distance: lesser the value more the similarity"""
        locDistance = Similarity.soergelsDistance(doc1['locations'], doc2['locations'])
        entDistance = Similarity.soergelsDistance(doc1['entities'], doc2['entities'])
        total = locDistance + entDistance
        if not locDistance:
            return False
        if total > threshold:
            return False
        combinedDistance = 0.5 * (cosineDistance) + 0.35 * locDistance + 0.15 * entDistance
        return combinedDistance

    def insertArticle(self, article, articleKey):
        if 'chainCand' in article:
            del article['chainCand']
        self.articleDb.insert(articleKey, article)
        return articleKey

    def insertChain(self, chainKey, chain, article):
        if article['protest']:
            chain['protest'] = True
        else:
            chain['protest'] = False
        chain['lastUpdateTime'] = article['publishedDate']
        chain['elements'].append(article['embersId'])
        if self.dirty_list:
            chain['elements'].sort()
            self.dirty_list = False
        self.chainDb.insert(chainKey, chain)
        return None

    def linkToChain(self, article):
        topChains = maxHeap(3, minChainThreshold)
        if not article['mostSimilar']:
            chainKey, chainProperties = self.createNewChain(article)
            yield chainKey, chainProperties
        else:
            chainIds = article['chainCand']
            try:
                chainProps = self.chainDb.getMany(chainIds)
            except Exception:
                log.warning('manually flushing db.')
                self.chainDb.flush()
                self.articleDb.flush()
                chainProps = self.chainDb.getMany(chainIds)

            chains = {c[0]: self.getChainSimilarity(article, c[1])
                      for c in chainProps}
            lastEle = {c[1]['elements'][-1]: k for k, c in chains.iteritems()}
            lastElemProp = self.articleDb.getMany(lastEle.values())
            for lkey, lprop in lastElemProp:
                if lkey['publishedDate'] > article['publishedDate']:
                    self.dirty_list = True
                distanceWithLastElement = self.calculateDistance(article['freqVector'], lprop['freqVector'])
                if distanceWithLastElement < minLastEleChainThreshold:
                    key = lastEle[lkey]
                    topChains.append(key, int(chains[key][0] * 100), chains[key][1])
            for cKey, cDistance, cProp in topChains:
                self.updateChain(copy.deepcopy(article), cProp)
                cProp['chainSize'] += 1
                dtStr = article['publishedDate'].strftime('%Y-%m-%d')
                if dtStr in cProp['letters']:
                    updateDictCount(cProp['letters'][dtStr], article['letters'])
                else:
                    cProp['letters'][dtStr] = copy.deepcopy(article['letters'])
                log.info('article added to chain %s with distance %s' % (cKey, cDistance))
                yield cKey, cProp
            if topChains.isEmpty():
                elements = {c: p[1]['elements'] for c, p in chains.iteritems()}
                branch_iter = self.createBranchStory(article, elements)
                for branchKey, branchProp in branch_iter:
                    log.info('branch chain added with key %s for article %s' % (branchKey, article['embersId']))
                    yield branchKey, branchProp
                #TODO: replace the above for loop with below line after testing
                #yield self.createBranchStory(article, topFive)

    def createBranchStory(self, article, chElements):
        chainSeen = set()
        for k, kProp in self.articleDb.getMany(article['mostSimilar'].keys()):
            branchKey, branchChain = self.createNewChain(article, k)
            self.updateChain(copy.deepcopy(kProp), branchChain)
            prevNode = article
            branchChain['elements'].append(k)
            uniqueArticles = set()
            [uniqueArticles.update(chElements[k]) for k in kProp['chainIds'] if k not in chainSeen and k in chElements]
            chainSeen.update(kProp['chainIds'])
            for elemKey, elemProp in self.articleDb.getMany(list(uniqueArticles)):
                if (article['publishedDate'] - elemProp['publishedDate']).days < 60:
                    distance = self.calculateDistance(article['freqVector'],
                                                      elemProp['freqVector'])
                    if distance < 0.8:
                        sum = self.getCombinedDistance(article, elemProp, distance, 1.9)
                        if sum < 0.9:
                            prevSimilarity = self.calculateDistance(elemProp['freqVector'],
                                                                    prevNode['freqVector'])
                            if prevSimilarity < 0.8:
                                prevNode = elemProp
                                self.updateChain(copy.deepcopy(elemProp), branchChain)
                                branchChain['elements'].append(elemKey)
            branchChain['elements'].sort()
            yield branchKey, branchChain

    def getChainSimilarity(self, article, cProp):
        entDistance = Similarity.soergelsDistance(cProp['entities'], article['entities'])
        locDistance = Similarity.soergelsDistance(cProp['locations'], article['locations'])
        chainSimilarity = 0.3 * entDistance + 0.7 * locDistance
        return (chainSimilarity, cProp)

    def updateChain(self, article, cProp):
        dtStr = article['publishedDate'].strftime('%Y-%m-%d')
        if dtStr in cProp['letters']:
            for item in article["entities"]:
                if not item in cProp["entities"]:
                    cProp["entities"][item] = article["entities"][item]
            for item in article["locations"]:
                if not item in cProp["locations"]:
                    cProp["locations"][item] = article["locations"][item]
            for k in article['letters']:
                if k not in cProp['letters'][dtStr]:
                    cProp['letters'][dtStr][k] = article["letters"][k]
                else:
                    cProp["letters"][dtStr][k] += article["letters"][k]
            return None
        if len(article['entities']) < 20:
            for item in cProp['entities']:
                if not item in article['entities']:
                    cProp['entities'][item] -= self.getTimeDecay(article['publishedDate'], cProp['lastUpdateTime'])
                    if cProp['entities'][item] < 0.0:
                        cProp['entities'][item] = 0.0
            for item in article['entities']:
                if not item in cProp['entities']:
                    cProp['entities'][item] = article['entities'][item]
                else:
                    cProp['entities'][item] += article['entities'][item]
        if len(article['locations']) < 15:
            for item in cProp['locations']:
                if not item in article['locations']:
                    timeDecay = self.getTimeDecay(article['publishedDate'], cProp['lastUpdateTime'])
                    cProp['locations'][item] -= timeDecay
                    if cProp['locations'][item] < 0.0:
                        cProp['locations'][item] = 0.0
            for item in article['locations']:
                if not item in cProp['locations']:
                    cProp['locations'][item] = article['locations'][item]
                else:
                    cProp['locations'][item] += article['locations'][item]
        cProp['letters'][dtStr] = copy.deepcopy(article['letters'])
        cProp['chainSize'] += 1
        return None

    def createNewChain(self, article, doc2Key=''):
        global CHAIN_NUM
        CHAIN_NUM += 1
        dtStr = article['publishedDate'].strftime('%Y-%m-%d')
        cKey = str(uuid.uuid5(uuid.NAMESPACE_DNS, ('_'.join([dtStr, article['embersId'], article['url'],
                                                             doc2Key])).encode('utf-8')))
        lettersKey = '_'.join([dtStr, '1'])
        cProp = {'elements': [], 'locations': copy.deepcopy(article['locations']), 'embersId': cKey,
                 'lastUpdateTime': copy.deepcopy(article['publishedDate']),
                 'entities': copy.deepcopy(article['entities']),
                 'letters': {lettersKey: copy.deepcopy(article['letters'])}, 'chainSize': 1}
        log.info('new chain %s created %s' % (cKey, CHAIN_NUM))
        return cKey, cProp

    def getTimeDecay(self, artDate, chainUpdatedDate, delta=7):
        return 1.0
        tDelta = (artDate - chainUpdatedDate).days
        noOfPeriods = tDelta / delta
        if noOfPeriods < 0.0:
            noOfPeriods = -noOfPeriods
        decay = logarithm(noOfPeriods + 1)
        if decay < 0.0:
            decay = -decay
        return decay


def main(args):
    reader = queue.open(args.sub)
    chainer = Chainer(args.config, args.dbname, args.dbaddr, args.buffersize,
                      args.simFunc)
    chain = chainer.chain
    try:
        cnt = 0
        for article in reader:
            cnt += 1
            print cnt
            chain(article)
    except KeyboardInterrupt:
        print "closing db connection"
        chainer.close()
    return None


def dictEquality(A, B):
    if len(A) != len(B):
        print "unequal length"
        return False
    for k in A:
        if k in B:
            if A[k] != B[k]:
                return False
        else:
            return False
    return True


if __name__ == "__main__":
    import os
    ap = args.get_parser()
    ap.add_argument('-c', '--config',
                    default=os.path.join(os.path.dirname(__file__),
                    'config-newsthreader'))
    ap.add_argument('--buffersize', type=int, default=20)
    ap.add_argument('--dbname', type=str)
    ap.add_argument('--dbaddr', type=str, default='localhost:9160')
    ap.add_argument('--simFunc', type=str, default='cosine')
    args = ap.parse_args()
    logs.init(args)
    main(args)
