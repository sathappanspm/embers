#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
    *.py: Description of what * does.
"""
from etool import args, logs, queue
from datetime import datetime
from utils import Vector, isodate, COUNTRY_REVERSE_CODES  # updateHandler
import os
__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = "NewsThreader"

log = logs.getLogger('%s-%s.log' % (__processor__, str(datetime.now())))
CONFIG_DIR = os.path.join(os.path.dirname(__file__))


def loc_To_locVector(article):
    """Function to convert location dict from geocoder containing offsets to freqVector
    :params: dictObj of structure {'loc': {'count': X, 'offset': Y}}
    """
    geoCodeEn = article['BasisEnrichment']['geoCodeEnrichment']
    locVector = {k: geoCodeEn['locations'][k]['count'] for k in geoCodeEn['locations']}
    locVector.update({k: geoCodeEn['states'][k]['count'] for k in geoCodeEn['states']})
    return locVector


def generateKey(article, publishedDate):
    """Function to generate embersId for the freqVector message"""
    countryCode = COUNTRY_REVERSE_CODES[article["country"]]
    dt = publishedDate.strftime('%Y-%m-%d')
    articleKey = '%s_%s_%s' % (countryCode, dt, article['embersId'])
    return articleKey


class STD_IO_HANDLER(object):
    """A class object to handle std_out and std_in in a similar fashion to queue write and read"""
    def __init__(self, nocaptureFlag):
        import sys
        import simplejson as json
        self.jLoads = json.loads
        self.jDumps = json.dumps
        self.reader = sys.stdin
        self.writer = sys.stdout
        if nocaptureFlag:
            self.write = self.nocaptureWrite

    def read(self):
        jLoads = self.jLoads
        for l in self.reader:
            try:
                j = jLoads(l.strip(), encoding='utf-8')
                yield j
            except ValueError:
                continue

    def write(self, article):
        self.writer.write(self.jDumps(article).encode('utf-8'))
        self.writer.write('\n')
        return

    def nocaptureWrite(self, article):
        pass


def main(args):
    """Main function"""
    logs.init(args)
    KEYWORD_THRESHOLD = args.keywordThreshold
    global CONFIG_DIR
    if args.config:
        CONFIG_DIR = args.config
    vector = Vector(CONFIG_DIR, args.logtf)
    std_io_handler = STD_IO_HANDLER(args.nocapture)
    if args.cat:
        reader = std_io_handler.read()
    else:
        reader = queue.open(args.sub, 'r')
    if not args.pub:
        write = std_io_handler.write
    else:
        write = queue.open(args.pub, 'w', capture=args.nocapture).write
    newsCnt = 0
    incmngcnt = 0
    for article in reader:
        incmngcnt += 1
        if args.newsonly and "blog" in article or not article['country']:
            continue
        if not article['BasisEnrichment']['language']:
            article['BasisEnrichment']['language'] = ''
        freqVector, letters = vector.vectorize(article['BasisEnrichment']['tokens'],
                                               article['BasisEnrichment']['language'])
        if not letters:
            log.info('article does not contain any letters : %s' % article['url'])
            continue
        if len(letters) < KEYWORD_THRESHOLD:
            log.info('article does not contain enough letters %s' % article['url'])
            continue
        #TODO: For Now, only mexico is being used.
        if not article['country'] == "mexico":
            continue
        newsCnt += 1
        try:
            article['persons'] = article['BasisEnrichment']['geoCodeEnrichment']['persons']
            article['persons'].update(article['BasisEnrichment']['geoCodeEnrichment']['organisations'])
            if 'published' in article:
                publishedDate = datetime.fromtimestamp(float(article['published']))
            else:
                publishedDate = isodate(article['publishedDate'] if 'publishedDate' in article else article['date'])
            if 'crawled' in article:
                crawled = datetime.fromtimestamp(float(article['crawled']) / 1000)
                if (publishedDate - crawled).days > 30:
                    log.warning('published and crawled dates are too different: %s ' % article['url'])
                updated = datetime.fromtimestamp(float(article['updated']))
            else:
                crawled = updated = publishedDate
            locVector = loc_To_locVector(article)
            enrichedFeed = {'url': article['url'], 'freqVector': freqVector, 'letters': letters,
                            'publishedDate': publishedDate.isoformat('T'),
                            'country': article['country'], 'locations': locVector,
                            'inferredLocation': article['inferredLocation'], 'reportedFrom':
                            article['reportedFrom'], 'entities': article["persons"],
                            'derivedFrom': [article['embersId']],
                            'embersId': generateKey(article, publishedDate),
                            'crawled': crawled.isoformat('T'),
                            'updated': updated.isoformat('T')}
            if 'derivedFrom' in article:
                enrichedFeed['derivedFrom'] = article['derivedFrom']['derivedIds']
            if 'protest' in article:
                enrichedFeed['protest'] = True
            else:
                enrichedFeed['protest'] = False
        except Exception, e:
            log.error("Crtical Exception %s " % str(e))
            print "Exception %s" % str(e)
            exit(0)
        write(enrichedFeed)
        print "incmg Cnt %s , Filtered Cnt %s" % (incmngcnt, newsCnt)


if __name__ == "__main__":
    import os
    ap = args.get_parser()
    ap.add_argument('--cat', default=False, action='store_true', help='Flag to use cat command to pipe input')
    ap.add_argument('--nocapture', default=False, action='store_true', help='Switch on Queue Capture')
    ap.add_argument('-c', '--config', default=os.path.join(os.path.dirname(__file__), 'config-newsthreader'),
                    type=str, help='Path to the Config dir')
    ap.add_argument('--cf', type=str, help='''Path to the configuration file (*.cfg). If present,
                    the configurations in the *.cfg file takes precedence over any other argument passed through
                    the command (cmd) line.''')
    ap.add_argument('--keywordThreshold', type=float, default=2.0,
                    help="keyword threshold . Possible values 1.0, 2.0, 3.0")
    ap.add_argument('--buffersize', type=float, default=1,
                    help="buffer size for casandra batch inserts")
    ap.add_argument('--newsonly', default=False, action='store_true',
                    help="Only use news for chaining")
    ap.add_argument('--logtf', default=False, action='store_true',
                    help="log tranform for TF")

    args = ap.parse_args()
    logs.init(args)
    try:
        main(args)
    except KeyboardInterrupt:
        print "Keyboard Interrupt closing"
        exit(0)
