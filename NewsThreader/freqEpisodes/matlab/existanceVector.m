function result = existanceVector(temp,chain,times,ts,te),
    i = 1;
    cnt = 0;
    tID = find(times>=ts & times<=te);
    n =size(chain,1);
    for t=1:length(tID),
        for v=1:n,
            if chain(v,tID(t)) == temp(i),
                cnt = cnt + 1;
                i = i + 1;
                break;
            end
        end
        if i>length(temp),
            break;
        end
    end
    if cnt==length(temp),
        result = 1;
    else
        result = 0;
    end
    return
end