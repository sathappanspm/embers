#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    dbHandler.py: This Script serves as an interface between the NewsThreader model
                  and the underlying database.
    Last Modified:Wed Jan  9 21:01:15 2013

    Database Used: Cassandra
    python Handler: pycassa v1.7.2
    Tutorials: http://pycassa.github.com/pycassa/tutorial.html#connection-pooling
"""

import pycassa
from pycassa.pool import ConnectionPool
import json
from utils.dateHandler import isodate

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"


class JsonMarshal(object):
    @staticmethod
    def encode(dictObj, datetimeKeys=[], encoding='utf-8'):
        for k in datetimeKeys:
            if k in dictObj:
                dictObj[k] = dictObj[k].isoformat('T')
        return {k: json.dumps(dictObj[k], encoding='utf-8') for k in dictObj}

    @staticmethod
    def decode(dictObj, datetimeKeys=[], encoding='utf-8'):
        jObj = {k: json.loads(dictObj[k], encoding='utf-8') for k in dictObj}
        for k in datetimeKeys:
            if k in dictObj:
                jObj[k] = isodate(jObj[k])
        return jObj


class dbConnect(object):
    def __init__(self, keyspace, host='localhost:9160'):
        self.pool = ConnectionPool(keyspace, [host])
        return None

    def getColumnFamily(self, colFamily):
        return columnFamily(self.pool, colFamily)

    def close(self):
        self.pool.dispose()

    def __exit__(self):
        self.pool.dispose()


class columnFamily(object):
    def __init__(self, dbCon, colFamilyName, batchInsertSize, datetimeKeys=[]):
        self.family = pycassa.columnfamily.ColumnFamily(dbCon.pool, colFamilyName)
        self.batch = self.family.batch(queue_size=batchInsertSize)
        self.datetimeKeys = datetimeKeys
        return

    def get(self, rowKey, columnNames=[]):
        if columnNames:
            return rowKey, JsonMarshal.decode(self.family.get(rowKey, columns=columnNames),
                                              self.datetimeKeys if any(True for k in self.datetimeKeys if k in columnNames) else [])
        return rowKey, JsonMarshal.decode(self.family.get(rowKey), self.datetimeKeys)

    def getMany(self, rowKeys, columnNames=[]):
        result = self.family.multiget(rowKeys, columns=columnNames, buffer_size=20)
        for k in result:
            yield {k: JsonMarshal.decode(result[k], self.datetimeKeys)}

    def getRange(self, startKey, endKey, bufferSize=50, columnNames=[]):
        if columnNames:
            rangeIter = self.family.get_range(start=startKey, finish=endKey,
                                              buffer_size=bufferSize, columns=columnNames)
        else:
            rangeIter = self.family.get_range(start=startKey, finish=endKey, buffer_size=bufferSize)
        for k, v in rangeIter:
            yield k, JsonMarshal.decode(v, self.datetimeKeys)

    def getByClause(self, searchClause):
        #expr1 = create_index_expression()
        #expr2 = create_index_expression()
        #clause = create_index_clause([expr1, expr2], count = 500)
        #for key, value in users.get_indexed_slices(clause):
        #    yield key, value
        #return
        pass

    def flush(self):
        self.batch.send()
        return None

    def insert(self, rowKey, valueJson):
        return self.batch.insert(rowKey, JsonMarshal.encode(valueJson, self.datetimeKeys))
