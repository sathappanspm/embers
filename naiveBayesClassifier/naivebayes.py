#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""

from __future__ import division
import nltk
from unidecode import unidecode
import json


__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = ""


class naive_bayes(object):
    def __init__(self, lang, labels, prior=None):
        self.stemmer = nltk.stem.snowball.SnowballStemmer('spanish')
        self.labels = {'p': labels[0], 'n': labels[1]}
        if prior:
            with open(prior) as f:
                self.prior = json.load(f)
        else:
            self.prior = {}
        return

    def select_features(self, pdist, ndist, fdist, t_len):
        """remove features that have equal probabilites for +ve class and -ve class"""
        for k in pdist:
            p_p_givenx = (pdist[k] + 1) / (fdist[k] + 2)
            if k in ndist:
                p_n_givenx = (ndist[k] + 1) / (fdist[k] + 2)
            else:
                p_n_givenx = 1 / (fdist[k] + 2)
            if p_p_givenx / p_n_givenx < 0.8 or (p_p_givenx / p_n_givenx) > 1.2:
                self.prior[k] = {'p': p_p_givenx, 'n': p_n_givenx}
        for k in ndist:
            p_n_givenx = (ndist[k] + 1) / (fdist[k] + 2)
            if k not in pdist:
                p_p_givenx = 1 / (fdist[k] + 2)
                if p_p_givenx / p_n_givenx < 0.8 or (p_p_givenx / p_n_givenx) > 1.2:
                    self.prior[k] = {'p': p_p_givenx, 'n': p_n_givenx}
        return

    def get_all_words(self, args):
        positive = []
        negative = []
        pTitles = []
        nTitles = []
        p_len = 0
        n_len = 0
        with open(args.p) as f:
            for k in f:
                if k.strip().replace('\n', ''):
                    p_len += 1
                    pTitles.append((k, 'protest'))
                    for w in k.split():
                        if len(w) > 2:
                            positive.append(unidecode(self.stemmer.stem(w.decode('utf-8'))).lower())
        pfreqdist = nltk.FreqDist(positive)
        with open(args.n) as f:
            for k in f:
                if k.strip().replace('\n', ''):
                    n_len += 1
                    nTitles.append((k, 'non-protest'))
                    for w in k.split():
                        if len(w) > 2:
                            negative.append(unidecode(self.stemmer.stem(w.decode('utf-8'))).lower())
        nfreqdist = nltk.FreqDist(negative)
        fdist = nltk.FreqDist(positive + negative)
        self.select_features(pfreqdist, nfreqdist, fdist, p_len + n_len)
        return set(fdist.keys()), pTitles, nTitles

    def get_features(self, title):
        feature = []
        for k in title[0].split():
            if len(k.strip()) > 2:
                w = self.stemmer.stem(k.strip())
                for f in self.prior.keys():
                    if f.lower() == unidecode(w.decode('utf-8')).lower():
                        feature.append(w)
        return feature

    def classify(self, title):
        features = self.get_features(title)
        p_prob = 0
        n_prob = 0
        for k in features:
            p_prob += self.prior[k]['p']
            n_prob += self.prior[k]['n']
        if p_prob > n_prob:
            return self.labels['p']
        elif n_prob > p_prob:
            return self.labels['n']
        else:
            return ''

    def evaluate(self, p, n):
        pn = 0
        nc = 0
        for k in p:
            c = self.classify(self.get_features(k))
            if c == 'non-protest':
                pn += 1
            elif not c:
                nc += 1
        nn = 0
        for k in n:
            c = self.classify(self.get_features(k))
            if c == 'protest':
                nn += 1
            elif not c:
                nc += 1

        print "true negative ", pn
        print "false positive", nn
        print "no class", nc
        print "total true %s false %s" % (len(p), len(n))
        return

    def dump(self, out=None):
        if out:
            with open(out, 'w') as f:
                f.write(json.dumps(self.prior, encoding="utf-8"))

    def print_features(self):
        import pprint
        pprint.pprint(self.prior)
        return


def main(args):
    if args.model:
        nb = naive_bayes('es', ['protest', 'non-protest'], args.model)
    if args.classify:
        with open(args.classify) as f:
            for l in f:
                try:
                    j = json.loads(l, encoding='utf-8')
                except Exception, e:
                    continue
                lbl = nb.classify(j['title'])
                if lbl == 'protest':
                    print j['url']

    if args.p and args.n:
        all_features, pTitles, nTitles = nb.get_all_words(args)
        nb.evaluate(pTitles, nTitles)
        if args.out:
            nb.dump(args.out)
        #nb.print_features()
    return


if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('--p', type=str, help="positive training set")
    ap.add_argument('--n', type=str, help='negative training set')
    ap.add_argument('--out', type=str, help='outFile')
    ap.add_argument('--classify', type=str, help='file to be classified')
    ap.add_argument('--model', type=str, default='nb_protest_classifier.json', help='Model File')
    args = ap.parse_args()
    main(args)
