# -*- coding:utf-8 -*-

import DbManager
import json
from Similarity import Similarity
from minQueue import minQueue
# json structure assumed is {embers_id:'',published_date:'',lang:'',featureVectors:{tokens:'',NEClass:{}}}


class Chainer:
  def __init__(time_delta,n,base_threshold):
     self.db = DbManager.DbManager()
     self.time_delta = float(time_delta)
     self.consideration_size = n
     self.base_threshold = base_threshold
  def findMostSimilar(j):
    
    start_id = j["country"].join('_'.join(j['lang'])).join('_'.join(j['published_date']))
    article_id = start_id.join('_'.join(j['embers_id']))
    stop_date = float(j['published_date']) - 3600.0*self.time_delta 
    stop_id = j["country"].join('_'.join(j['lang'])).join('_'.join(str(stop_date)))
    
    scanner = self.db.Scanner(start_id,stop_id)

    minq = minQueue(self.consideration_size)


    try:
      next_article = scanner.next()
      while True:
        distance = Similarity.cosine_similarity(j['featureVectors']['tokens'],next_article['tokens'])
        if distance < 0.3 :
          return False

        if distance < self.base_threshold:
          combined_distance = self.get_combinedDistance(j['featureVectors']['tokens'],next_article['tokens'])
          minq.add(next_article['id'] , combined_distance)       
    except StopIteration:
      storyQ = minQueue(self.consideration_size - 2) 
      try:
        storyGenerator = self.db.get_topStories(minq)
        while True:
          next_story = storyGenerator.next()
          entity_distance = Similarity.jaccard_distance(j['featureVectors']['NER'],next_story['NER'])
          location_distance = Similarity.jaccard_distance(j['featureVectors']['location'],next_story['locations'])
          combined_distance = self.alpha * next_story['cosine_distance'] + self.beta * entity_distance + self.gamma * location_distance
          storyQ.add(story_id , combined_distance)
       except StopIteration:
          if storyQ.isEmpty():
            create_BranchStory()
          else:
            
            ##chainoutof minq
            ##
            addTochain(storyQ,j);
        

      

    self.db.insert(j)

