#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    dbHandler.py: This Script serves as an interface between the NewsThreader model
                  and the underlying database.
    Last Modified:Wed Jan  9 21:01:15 2013

    Database Used: Cassandra
    python Handler: pycassa v1.7.2
    Tutorials: http://pycassa.github.com/pycassa/tutorial.html#connection-pooling
"""

import pycassa
from pycassa.pool import ConnectionPool
import simplejson as json
from utils import isodate
import time
from pycassa.index import create_index_expression, create_index_clause, GT
import re

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

dictRE = re.compile('^[0-9]+$|^[\{|\[][^\}\]]*[\}|\]]')

def jLoads(dictObj):
    if dictObj == '':
        return ""
    if isinstance(dictObj, unicode) or isinstance(dictObj, str):
        if dictRE.match(dictObj):
            return json.loads(str(dictObj), encoding='utf-8')
    return dictObj


def jDumps(dictObj):
    if isinstance(dictObj, dict) or isinstance(dictObj, list):
        return json.dumps(dictObj, encoding='utf-8')
    return dictObj


class JsonMarshal(object):

    @staticmethod
    def encode(dictObj, datetimeKeys=[], encoding='utf-8'):
        for k in datetimeKeys:
            if k in dictObj:
                dictObj[k] = dictObj[k].isoformat('T')
        return {k: jDumps(dictObj[k]) for k in dictObj}

    @staticmethod
    def decode(dictObj, datetimeKeys=[], encoding='utf-8'):
        jObj = {k: jLoads(dictObj[k]) for k in dictObj}
        for k in datetimeKeys:
            if k in dictObj:
                jObj[k] = isodate(jObj[k])
        return jObj


class dbConnect(object):
    def __init__(self, keyspace, host='localhost:9160'):
        self.pool = ConnectionPool(keyspace, [host])
        return None

    def getColumnFamily(self, colFamily, batchInsertSize):
        return columnFamily(self, colFamily, batchInsertSize)

    def close(self):
        self.pool.dispose()

    def __exit__(self):
        self.pool.dispose()


class columnFamily(object):
    def __init__(self, dbCon, colFamilyName, batchInsertSize, datetimeKeys=[]):
        self.family = pycassa.columnfamily.ColumnFamily(dbCon.pool, colFamilyName)
        self.batch = self.family.batch(queue_size=batchInsertSize)
        self.datetimeKeys = datetimeKeys
        self.retry = False
        return

    def get(self, rowKey, columnNames=[]):
        if columnNames:
            return rowKey, JsonMarshal.decode(self.family.get(rowKey, columns=columnNames),
                                                  self.datetimeKeys if any(True for k in self.datetimeKeys if k in columnNames) else [])
        return rowKey, JsonMarshal.decode(self.family.get(rowKey), self.datetimeKeys)

    def getMany(self, rowKeys, columnstart='', columnfinish=''):
        result = self.family.multiget(rowKeys, column_start=columnstart,
                                      column_finish=columnfinish, buffer_size=20,
                                      column_count=10000, column_reversed=True)
        for k in result:
            yield k, JsonMarshal.decode(result[k], self.datetimeKeys)

    def getRange(self, startKey, endKey, bufferSize=50, columnNames=[]):
        if columnNames:
            rangeIter = self.family.get_range(start=startKey, finish=endKey,
                                              buffer_size=bufferSize, columns=columnNames)
        else:
            rangeIter = self.family.get_range(start=startKey, finish=endKey, buffer_size=bufferSize)
        for k, v in rangeIter:
            yield k, JsonMarshal.decode(v, self.datetimeKeys)

    def getByClause(self, country, dateStr):
        country_expr = create_index_expression('country', 'mexico')
        date_expr = create_index_expression('publishedDate', dateStr, GT)
        clause = create_index_clause([country_expr, date_expr], count=10000)
        for k, v in self.family.get_indexed_slices(clause):
            yield k, JsonMarshal.decode(v, self.datetimeKeys)

    def flush(self):
        self.batch.send()
        return None

    def insert(self, rowKey, valueJson):
        return self.batch.insert(rowKey, JsonMarshal.encode(valueJson, self.datetimeKeys))

    def batch_insert(self, rows):
        return self.family.batch_insert(rows)
