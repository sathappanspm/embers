#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
    Last Modified:
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

import csv
import json
from unidecode import unidecode
import os
from etool import logs

WG_DATA = os.path.join(os.path.dirname(__file__), "wg-partial.txt")

WG_FIELDS = ["id", "name", "alt_names", "orig_names", "type", "pop",
             "longitude",
             "latitude",
             "country", "admin1", "admin2", "admin3"]
CITY_DATA_FIELDS = ["country", "admin1", "city", "variants", "coodinates"]

logs.init()
log = logs.getLogger('GEO')


class Geo(object):
    def __init__(self, WG_DATA, CITY_DATA):
        if WG_DATA:
            with open(WG_DATA, 'r') as f:
                dialect = csv.Sniffer().sniff(f.read(), delimiters='\t')
                f.seek(0)
                reader = csv.DictReader(f, dialect=dialect, fieldnames=WG_FIELDS)
                self.wg_data = {'country': {'brasil': 'brazil'}, 'state': {}}
                for k in reader:
                    city = unidecode(k['name'].decode('utf-8').lower().strip())
                    cntry = unidecode(k['country'].decode('utf-8').lower().strip())
                    state = unidecode(k['admin1'].decode('utf-8').lower().strip())
                    if not cntry in self.wg_data['country']:
                        self.wg_data['country'][cntry] = cntry
                    if not state:
                        print "error"
                    admins = [state]
                    if k['admin2']:
                        admin2 = unidecode(k['admin2'].decode('utf-8').strip().lower())
                        admins.append(admin2)
                    if k['admin3']:
                        admin3 = unidecode(k['admin3'].decode('utf-8').strip().lower())
                        admins.append(admin3)
                    for admin in admins:
                        if admin in self.wg_data['state']:
                            if cntry not in self.wg_data['state'][admin]:
                                self.wg_data['state'][admin].append(cntry)
                        else:
                            self.wg_data['state'][admin] = [cntry]
                    if city in self.wg_data:
                        if cntry in self.wg_data[city]:
                            if not state in self.wg_data[city][cntry]:
                                self.wg_data[city][cntry][state] = city
                            else:
                                pass
                        else:
                            self.wg_data[city][cntry] = {state: city}
                    else:
                        self.wg_data[city] = {cntry: {state: city}}
                    variants = [unidecode(r.decode('utf-8').lower().strip()) for r in k['alt_names'].split(',')]
                    for alias in variants:
                        if alias in self.wg_data:
                            if cntry in self.wg_data[alias]:
                                if state in self.wg_data[alias][cntry]:
                                    self.wg_data[alias][cntry][state] = city
                                else:
                                    self.wg_data[alias][cntry][state] = city
                            else:
                                self.wg_data[alias][cntry] = {state: city}
        with open(CITY_DATA, 'w') as out:
            out.write(json.dumps(self.wg_data).encode('utf-8'))
        print "cities in wg_data %s " % (len(self.wg_data) - 2)
        print "countries in wg_data %s" % (len(self.wg_data['country']))
        print "states in wg_data %s" % (len(self.wg_data['state']))
        #if CITY_DATA:
        #    with open(CITY_DATA, 'r') as f:
        #        dialect2 = csv.Sniffer().sniff(f.read(), delimiters='|')
        #        f.seek(0)
        #        reader2 = csv.DictReader(f, dialect=dialect2, fieldnames=CITY_DATA_FIELDS)
        #        for k in reader2:
        #            city = unidecode(k['city'].lower()).strip()
        #            cntry = unidecode(k['country'].lower()).strip()
        #            state = unidecode(k['admin1'].lower()).strip()
        #            aliases = [unidecode(r.strip()).lower() for r in k['variants'].split(',')]
        #            if city in self.wg_data:
        #                if cntry in self.wg_data[city]:
        #                    if not state in self.wg_data[city][cntry] or not state in self.admin2_3[city]:
        #                        log.error('%s -- not found in state %s-%s' % (city, cntry, state))
        #                else:
        #                    log.error('%s not found in country %s' % (city, cntry))
        #            else:
        #                log.error('%s not found in wg_data' % city)
        #            if '\\n' in aliases:
        #                continue
        #            for alias in aliases:
        #                if alias in self.wg_data:
        #                    if cntry in self.wg_data[alias]:
        #                        if state in self.wg_data[alias][cntry]:
        #                            if city != self.wg_data[alias][cntry][state]:
        #                                log.error('alias-%s does not match city %s' % (alias, city))
        #                        elif not state in self.admin2_3[alias]:
        #                            log.error('alias %s not in state-%s' % (alias, state))
        #                    else:
        #                        log.error('alias %s not in country %s' % (alias, cntry))
        #                else:
        #                    log.error('alias %s not at all in list %s-%s' % (alias, city, cntry))
        return


if __name__ == "__main__":
    geo = Geo('wg-partial.txt', 'wg_data.txt')
