#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""
from __future__ import division
from unidecode import unidecode
import codecs
import re
import simplejson as json
#from collections import Counter
from math import log as logarithm, sqrt
from datetime import datetime
import pdb
#from itertools import imap
from etool import logs
from numpy import array as nparray, abs as vector_abs, maximum as vector_max
from numpy.linalg import norm as vector_norm

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = "utils.py"

log = logs.getLogger(__processor__)
stringRE = re.compile('[^a-zA-Z\s_]+')
dateRE = re.compile('\d\d\d\d-\d\d-\d\d')
calculate_length = lambda x, y: x + y ** 2
calculate_tfidf = lambda x: (x[0], x[1] * getIdf(x[0]))
#getIdf = lambda x: IDF['TOKENS'][x] if x in IDF else IDF['maxIdf']
IDF_FOLDER = ''
DOC_FREQ_FOLDER = ''
IDF = {'TOKENS': {}, 'maxIdf': 1.0}
DOC_FREQ = {'TOKENS': {}, 'docCount': 0.0}
COUNTRY_CODES = {'ar': "Argentina", 'bz': 'Belize', 'bo': 'Bolivia',
                 'cl': 'Chile', 'co': 'Colombia', 'cr': 'Costa Rica',
                 'ec': 'Ecuador', 'sv': 'El Salvador', 'gf': 'French Guiana',
                 'gt': 'Guatemala', 'gy': 'Guyana', 'hn': 'Honduras',
                 'ni': 'nicaragua', 'pa': 'Panama', 'py': 'Paraguay',
                 'pe': 'Peru', 'sr': 'Suriname', 'uy': 'Uruguay',
                 've': 'Venezuela', 'br': 'Brazil', 'mx': "Mexico"
                 }
COUNTRY_REVERSE_CODES = {COUNTRY_CODES[k].lower(): k for k in COUNTRY_CODES}
IGNORE_PERCENT = 0.9


def getIdf(x):
    if x in IDF['TOKENS']:
        if IDF['TOKENS'][x] < 0.105:
            return 0.0
        return IDF['TOKENS'][x]
    else:
        sum_idf = 0
        wrds = x.split()
        for k in wrds:
            if len(k) > 2:
                if k in IDF['TOKENS']:
                    sum_idf += IDF['TOKENS'][k]
                else:
                    log.error('k not in idf %s' % k)
        if sum_idf:
            IDF['TOKENS'][x] = sum_idf / len(wrds)
            return IDF['TOKENS'][x]
        else:
            log.error('returning MAX-IDF for word %s' % x)
            IDF['TOKENS'][x] = IDF['maxIdf']
            return IDF['maxIdf']


def initializeIDF(CONFIG_DIR):
    global IDF_FOLDER, DOC_FREQ_FOLDER, IDF, DOC_FREQ
    IDF_FOLDER = '%s/%s' % (CONFIG_DIR, 'idf')
    DOC_FREQ_FOLDER = "%s/%s" % (CONFIG_DIR, 'docFreq')
    import os
    idfFiles = os.listdir(IDF_FOLDER)
    idfFiles.sort()
    with codecs.open('%s/%s' % (IDF_FOLDER, idfFiles[-1]), 'r', encoding='utf-8') as f:
        IDF = json.load(f)
    docFreqFiles = os.listdir(DOC_FREQ_FOLDER)
    docFreqFiles.sort()
    with codecs.open('%s/%s' % (DOC_FREQ_FOLDER, docFreqFiles[-1]), 'r', encoding='utf-8') as f:
        DOC_FREQ = json.load(f)
    log.info('Using idf resource : %s and docFreq resource : %s' % (idfFiles[-1], docFreqFiles[-1]))
    return


def isodate(date):
    dt = None
    try:
        dt = datetime.strptime(re.findall(dateRE, date)[0], '%Y-%m-%d')
    except ValueError, e:
        log.error('date could not be decoded: %s, %s' % (date, e))
    return dt


def updateDictCount(dict1, dict2):
    for key in dict2:
        if key in dict1:
            dict1[key] += dict2[key]
        else:
            dict1[key] = dict2[key]
    return dict1


def normalize_str(s):
    if isinstance(s, str):
        return unidecode(s.decode('utf-8').strip()).lower()
    return unidecode(s.strip()).lower()


class Vector(object):
    def __init__(self, config, logTf=False, combine=False):
        self.STOP_WORDS = {}
        if combine:
            self.vectorize = self.vectorizeCombined
        self.combine = combine
        self.logtf = logTf
        self.langSet = set(("spanish", "english", "portuguese"))
        with open("%s/keyword_dict.txt" % config) as f:
            self.KEYWORDS = json.load(f, encoding='utf-8')
            self.KEYWORDS['spanish'] = set([normalize_str(k) for k in self.KEYWORDS['spanish']])
            self.KEYWORDS['english'] = set([normalize_str(k) for k in self.KEYWORDS['english']])
            self.KEYWORDS['portuguese'] = set([normalize_str(k) for k in self.KEYWORDS['portuguese']])
            if combine:
                self.KEYWORDS['combined'] = set(self.KEYWORDS['spanish'] + self.KEYWORDS['english'] + self.KEYWORDS['portuguese'])
        for l in self.langSet:
            with codecs.open("%s/stopwords/%s.txt" % (config, l), 'r',
                             encoding='utf-8') as f:
                self.STOP_WORDS[l] = set([normalize_str(k) for k in f])

    def vectorizeCombined(self, tokens, lang=''):
        lang = lang.lower()
        if not lang in self.langSet:
            lang = 'spanish'
        fDist = {}
        alphabets = {}
        tokenLength = len(tokens)
        for t in tokens[0: int(IGNORE_PERCENT * tokenLength)]:
            if t['lemma']:
                word = normalize_str(t['lemma'])
            else:
                word = normalize_str(t['value'])
            if ' ' in word:
                wrds = word.split()
            elif '_' in word:
                wrds = word.split('-')
            elif '-' in word:
                wrds = word.split('_')
            else:
                wrds = word
            for wrd in wrds:
                word = stringRE.sub('', wrd)
                if len(word) > 1 and word not in self.STOP_WORDS[lang]:
                    if word in self.KEYWORD["combined"]:
                        if word in alphabets:
                            alphabets[word] += 1
                        else:
                            alphabets[word] = 1
                    if word in fDist:
                        fDist[word] += 1
                    else:
                        fDist[word] = 1
        return fDist, alphabets

    def vectorize(self, tokens, lang=''):
        lang = lang.lower()
        if lang not in self.langSet:
            lang = "spanish"
        #if self.combine:
        #    kwLang = "combined"
        #else:
        #    kwLang = lang
        fDist = {}
        tokenLength = len(tokens)
        for t in tokens[: int(IGNORE_PERCENT * tokenLength)]:
            if t['lemma']:
                word = normalize_str(t['lemma'])
            else:
                word = normalize_str(t['value'])
            if ' ' in word:
                wrds = word.split()
            elif '_' in word:
                wrds = word.split('-')
            elif '-' in word:
                wrds = word.split('_')
            else:
                wrds = word
            for wrd in wrds:
                word = stringRE.sub('', wrd)
                if len(word) > 1 and word not in self.STOP_WORDS[lang]:
                    #fDist[word] += 1
                    #if word in self.KEYWORDS[lang]:
                    #    #alphabets[word] += 1
                    #    if word in alphabets:
                    #        alphabets[word] += 1
                    #    else:
                    #        alphabets[word] = 1
                    if word in fDist:
                        fDist[word] += 1
                    else:
                        fDist[word] = 1
        if self.logtf:
            fDist = {k: logarithm(fDist[k] + 1) for k in fDist}
        alphabets = {k: fDist[k] for k in fDist if k in self.KEYWORDS[lang]}
        return fDist, alphabets


def freqNormalization(fdist):
    """Normalizes a token frequencies within a document, through division by max (Infinite norm).
    Keyword arguments:
        fdist -- A frequency dict of tokens in text.Frequencies have to be float.
    """
    v = fdist.values()
    if v != []:
        max_val = max(v)
        fdist_normalized = dict(map(lambda x: (x[0], x[1] / max_val), fdist.iteritems()))
        return fdist_normalized
    return fdist


def cosineNormalization(fdist):
    """Normalizes vector by dividing by every dimension value by length of vector (Euclidean norm).
    Keyword arguments:
        fdist -- A frequency dict of tokens in text.Frequencies have to be float.
    """
    #calculate_length = lambda x, y: x + y ** 2
    fdist_norm = sqrt(reduce(calculate_length, fdist.values(), 0))
    fdist_normalized = dict(map(lambda x: (x[0], x[1] / fdist_norm), fdist.iteritems()))
    return fdist_normalized


class Similarity:
    @staticmethod
    def cosineSimilarity(doc1, doc2):
        doc1_tfidf = cosineNormalization(dict(map(calculate_tfidf, doc1.iteritems())))
        doc2_tfidf = cosineNormalization(dict(map(calculate_tfidf, doc2.iteritems())))
        iterDoc = doc1_tfidf
        otherDoc = doc2_tfidf
        if len(doc2_tfidf) < len(doc1_tfidf):
            iterDoc = doc2_tfidf
            otherDoc = doc1_tfidf
        dot_product = sum([doc1_tfidf[w] * doc2_tfidf[w] for w in iterDoc if w in otherDoc])
        #print dot_product
        #doc1_tfidf = dict(map(calculate_tfidf, doc1.iteritems()))
        #doc2_tfidf = dict(map(calculate_tfidf, doc2.iteritems()))
        #keySet = list(set(doc1.keys() + doc2.keys()))
        #vec1 = nparray([doc1_tfidf[k] if k in doc1_tfidf else 0.0 for k in keySet])
        #vec2 = nparray([doc2_tfidf[k] if k in doc2_tfidf else 0.0 for k in keySet])
        #dot_product = vector_dotproduct(vec1, vec2) / (vector_norm(vec1) * vector_norm(vec2))
        return dot_product

    @staticmethod
    def jaccardSimilarity(ner1, ner2, duplicateThreshold=0.8, doIdf=True):
        if doIdf:
            ner1_tfidf = dict(map(calculate_tfidf, ner1.iteritems()))
            ner2_tfidf = dict(map(calculate_tfidf, ner2.iteritems()))
        else:
            ner1_tfidf = ner1
            ner2_tfidf = ner2
        ner1_tfidf = cosineNormalization(ner1_tfidf)
        ner2_tfidf = cosineNormalization(ner2_tfidf)
        dot_product = sum([ner1_tfidf[w] * ner2_tfidf[w] for w in ner1_tfidf if w in ner2_tfidf])
        jaccardDistance = dot_product / (2.0 - dot_product)
        if jaccardDistance > duplicateThreshold:
            return -1.0
        return jaccardDistance

    @staticmethod
    def soergelsDistance(ner1, ner2):
        """ sum(Ai -Bi, for all common i's) + Aj's + Bk's divided by
            sum(max(A,B))
        """
        if ner1 and ner2:
            ner1_tfidf = dict(map(calculate_tfidf, ner1.iteritems()))
            ner2_tfidf = dict(map(calculate_tfidf, ner2.iteritems()))
            keyset = list(set(ner1.keys() + ner2.keys()))
            vec1 = nparray([ner1_tfidf[k] if k in ner1_tfidf else 0.0 for k in keyset])
            vec2 = nparray([ner2_tfidf[k] if k in ner2_tfidf else 0.0 for k in keyset])
            vec1 = vec1 / vector_norm(vec1)
            vec2 = vec2 / vector_norm(vec2)
            distance = sum(vector_abs(vec1 - vec2)) / sum(vector_max(vec1, vec2))
            return distance
            #common_max = ()
            #common_diff = ()
            #ner1_unique = ()
            #for k, v in ner1_tfidf.iteritems():
            #    if k in ner2_tfidf:
            #        common_max += (max(v, ner2_tfidf[k]),)
            #        common_diff += (abs(v - ner2_tfidf[k]),)
            #    else:
            #        ner1_unique += (v,)

            #common = [(ner1_tfidf[k], ner2_tfidf[k]) for k in ner1_tfidf if k in ner2_tfidf]
            #common_max = map(max, common)
            #common_diff = map(lambda x: abs(x[0] - x[1]), common)
            #ner2_unique = [v for k, v in ner2_tfidf.iteritems() if k not in ner1_tfidf]
            #ner1_unique = [ner1_tfidf[k] for k in ner1_tfidf if k not in ner2_tfidf]
            #unique_sum = sum(ner1_unique) + sum(ner2_unique)
            #return (sum(common_diff) + unique_sum) / (sum(common_max) + unique_sum)
        else:
            return 1.0


class updateHandler(object):
    def updateIDFTables(self):
        from datetime import datetime
        idftokens = {}
        IDF = {'maxIdf': 0.0, 'TOKENS': idftokens}
        docCount = DOC_FREQ['docCount']
        tokens = DOC_FREQ['TOKENS']
        maxIdf = 0.0
        for word in tokens:
            idfWord = logarithm(docCount / (1 + tokens[word]))
            if idfWord > maxIdf:
                maxIdf = idfWord
            IDF['TOKENS'][word] = idfWord
        IDF['maxIdf'] = maxIdf
        with codecs.open("%s/idf_%s" % (IDF_FOLDER, datetime.now().isoformat('T')), 'w', encoding='utf-8') as f:
            f.write(json.dumps(IDF, encoding='utf-8'))
        with codecs.open("%s/docFreq_%s" % (DOC_FREQ_FOLDER, datetime.now().isoformat('T')), 'w', encoding='utf-8') as f:
            f.write(json.dumps(DOC_FREQ, encoding='utf-8'))
        return None

    def updateDocFreqCounts(self, article):
        tokens = DOC_FREQ['TOKENS']
        DOC_FREQ['docCount'] += 1.0
        for k in article['freqVector']:
            if k in tokens:
                tokens[k] += 1.0
            else:
                tokens[k] = 1.0
        for k in article['BasisEnrichment']['geoCodeEnrichment']['locations']:
            if k in tokens:
                tokens[k] += 1.0
            else:
                tokens[k] = 1.0
        for k in article['BasisEnrichment']['geoCodeEnrichment']['persons']:
            if k in tokens:
                tokens[k] += 1.0
            else:
                tokens[k] = 1.0
        return


class minHeap(object):
    """stores values greater than a threshold, has a min bound.
        Min Element is thrown out, when a better one is found.
    """
    def __init__(self, size, threshold):
        self.size = size
        self.eMap = {}  # {distance: entry}
        self.propertyMap = {}
        self.threshold = threshold
        self.top = 1

    def append(self, key, distance, prop=None):
        if distance < self.threshold:
            return
        if distance in self.eMap:
            self.eMap[distance] += (key,)
            self.propertyMap[key] = prop
            return
        if len(self.propertyMap) < self.size:
            self.eMap[distance] = (key,)
            self.propertyMap[key] = prop
            if distance < self.top:
                self.top = distance
            return
        if distance < self.top:
            return
        if distance > self.top:
            for k in self.eMap[self.top]:
                del self.propertyMap[k]
            del self.eMap[self.top]
            self.eMap[distance] = (key,)
            self.propertyMap[key] = prop
            self.top = min(self.eMap)

    def __iter__(self):
        for distance in self.eMap:
            for key in self.eMap[distance]:
                yield key, distance, self.propertyMap[key]

    def isEmpty(self):
        return False if self.eMap else True

    def pprint(self):
        for k in self.propertyMap:
            print self.propertyMap[k]['url']
        return


class maxHeap(object):
    """Stores values less  than a threshold...has a max bound.
       Max element is thrown out in case of pop
    """
    def __init__(self, size, threshold):
        self.size = size
        self.eMap = {}  # {distance: entry}
        self.propertyMap = {}
        self.threshold = threshold
        self.top = 0

    def append(self, key, distance, prop=None):
        if distance > self.threshold:
            return
        if distance in self.eMap:
            self.eMap[distance] += (key,)
            if key in self.propertyMap:
                print "error key already in Map", key
            self.propertyMap[key] = prop
            return
        if len(self.propertyMap) < self.size:
            self.eMap[distance] = (key,)
            self.propertyMap[key] = prop
            if self.top < distance:
                self.top = distance
            return
        if distance > self.top:
            return
        if distance < self.top:
            try:
                for k in self.eMap[self.top]:
                    del self.propertyMap[k]
            except Exception:
                print k
                pdb.set_trace()
            del self.eMap[self.top]
            self.eMap[distance] = (key,)
            self.propertyMap[key] = prop
            self.top = max(self.eMap)

    def __iter__(self):
        for distance in self.eMap:
            for key in self.eMap[distance]:
                yield key, distance, self.propertyMap[key]

    def isEmpty(self):
        return False if self.eMap else True

    def pprint(self):
        for k in self.eMap:
            print k
        for k in self.propertyMap:
            print self.propertyMap[k]['url']
        return
