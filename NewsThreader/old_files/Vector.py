#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""
from unidecode import unidecode
import codecs
import re
import json
from math import log as logarithm, sqrt
from collections import OrderedDict
from datetime import datetime
from etool import logs

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = "utils.py"

log = logs.getLogger(__processor__)
stringRE = re.compile('[^a-zA-Z\s_]+')
dateRE = re.compile('\d\d\d\d-\d\d-\d\d', re.I)
calculate_length = lambda x, y: x + y ** 2
calculate_tfidf = lambda x: (x[0], x[1] * getIdf(x[0]))
getIdf = lambda x: float(IDF[x]) if x in IDF else IDF['maxIdf']
IDF_FOLDER = ''
DOC_FREQ_FOLDER = ''
IDF = {}
DOC_FREQ = {}
COUNTRY_CODES = {'ar': "Argentina", 'bz': 'Belize', 'bo': 'Bolivia',
                 'cl': 'Chile', 'co': 'Colombia', 'cr': 'Costa Rica',
                 'ec': 'Ecuador', 'sv': 'El Salvador', 'gf': 'French Guiana',
                 'gt': 'Guatemala', 'gy': 'Guyana', 'hn': 'Honduras',
                 'ni': 'nicaragua', 'pa': 'Panama', 'py': 'Paraguay',
                 'pe': 'Peru', 'sr': 'Suriname', 'uy': 'Uruguay',
                 've': 'Venezuela', 'br': 'Brazil', 'mx': "Mexico"
                 }
COUNTRY_REVERSE_CODES = {COUNTRY_CODES[k].lower(): k for k in COUNTRY_CODES}


def initializeIDF(CONFIG_DIR):
    global IDF_FOLDER, DOC_FREQ_FOLDER, IDF, DOC_FREQ
    IDF_FOLDER = '%s/%s' % (CONFIG_DIR, 'idf')
    DOC_FREQ_FOLDER = "%s/%s" % (CONFIG_DIR, 'docFreq')
    import os
    idfFiles = os.listdir(IDF_FOLDER)
    idfFiles.sort()
    with codecs.open('/'.join([IDF_FOLDER, idfFiles[-1]]), 'r', encoding='utf-8') as f:
        IDF = json.load(f)
    docFreqFiles = os.listdir(DOC_FREQ_FOLDER)
    docFreqFiles.sort()
    with codecs.open('/'.join([DOC_FREQ_FOLDER, docFreqFiles[-1]]), 'r', encoding='utf-8') as f:
        DOC_FREQ = json.load(f)


def isodate(date):
    dt = None
    try:
        dtStr = re.findall(dateRE, date)
        if dtStr:
            dt = datetime.strptime(dtStr[0].strip(), '%Y-%m-%d')
        else:
            dt = datetime.strptime(date.strip(), '%Y-%m')
    except ValueError:
        try:
            dt = datetime.strptime(date.strip(), '%Y')
        except ValueError, e:
            log.error('date could not be decoded: %s, %s' % (date, e))
    if dt:
        return dt
    return None


def normalize_str(string):
    if type(string) == str:
        return unidecode(string.decode('utf-8'))
    return unidecode(string)


class Vector(object):
    def __init__(self, config, combine=False):
        self.STOP_WORDS = {}
        self.combine = combine
        self.langSet = set(["spanish", "english", "portuguese"])
        with open("%s/keyword_dict.txt" % config) as f:
            if combine:
                self.KEYWORDS['combined'] = set(self.KEYWORDS['es'] + self.KEYWORDS['en'] + self.KEYWORDS['pt'])
            self.KEYWORDS = json.load(f, encoding='utf-8')
            self.KEYWORDS['spanish'] = set([unidecode(k).lower() for k in self.KEYWORDS['spanish']])
            self.KEYWORDS['english'] = set([unidecode(k).lower() for k in self.KEYWORDS['english']])
            self.KEYWORDS['portuguese'] = set([unidecode(k).lower() for k in self.KEYWORDS['portuguese']])
        languages = ['english', 'spanish', 'portuguese']
        for l in languages:
            with codecs.open("%s/stopwords/%s.txt" % (config, l), 'r',
                             encoding='utf-8') as f:
                self.STOP_WORDS[l] = set([unidecode(k.strip()) for k in f])

    def vectorize(self, tokens, lang):
        if lang is None or lang.lower() not in self.langSet:
            lang = "spanish"
        if self.combine:
            kwLang = "combined"
        else:
            kwLang = lang.lower()
        fDist = {}
        alphabets = OrderedDict({})
        for t in tokens:
            word = stringRE.sub('', unidecode(t['lemma'].lower()))
            if word and len(word) > 1 and word not in self.STOP_WORDS[lang.lower()]:
                if word in self.KEYWORDS[kwLang]:
                    if word not in alphabets:
                        alphabets[word] = 0
                    alphabets[word] += 1
                if word in fDist:
                    fDist[word] += 1
                else:
                    fDist[word] = 1
        return fDist, alphabets


class Normalization(object):
    @staticmethod
    def freqNormalization(fdist):
        """Normalizes a token frequencies within a document, through division by max (Infinite norm).
        Keyword arguments:
            fdist -- A frequency dict of tokens in text.Frequencies have to be float.
        """
        v = fdist.values()
        if v != []:
            max_val = max(v)
            fdist_normalized = dict(map(lambda x: (x[0], x[1] / max_val), fdist.iteritems()))
            return fdist_normalized
        return fdist

    @staticmethod
    def cosineNormalization(fdist):
        """Normalizes vector by dividing by every dimension value by length of vector (Euclidean norm).
        Keyword arguments:
            fdist -- A frequency dict of tokens in text.Frequencies have to be float.
        """
        calculate_length = lambda x, y: x + y ** 2
        fdist_norm = sqrt(reduce(calculate_length, fdist.values(), 0))
        fdist_normalized = dict(map(lambda x: (x[0], float(x[1]) / fdist_norm), fdist.iteritems()))
        return fdist_normalized


class Similarity:
    @staticmethod
    def cosineSimilarity(doc1, doc2, duplicateThreshold=0.8):
        doc1_tfidf = Normalization.cosineNormalization(dict(map(calculate_tfidf, doc1.iteritems())))
        doc2_tfidf = Normalization.cosineNormalization(dict(map(calculate_tfidf, doc2.iteritems())))
        dot_product = sum([doc1_tfidf[w] * doc2_tfidf[w] for w in doc1_tfidf if w in doc2_tfidf])
        if dot_product > duplicateThreshold:
            return -1.0
        return dot_product

    @staticmethod
    def jaccardSimilarity(ner1, ner2, duplicateThreshold=0.8, doIdf=True):
        if doIdf:
            ner1_tfidf = dict(map(calculate_tfidf, ner1.iteritems()))
            ner2_tfidf = dict(map(calculate_tfidf, ner2.iteritems()))
        else:
            ner1_tfidf = ner1
            ner2_tfidf = ner2
        ner1_tfidf = Normalization.cosine_normalize(ner1_tfidf)
        ner2_tfidf = Normalization.cosine_normalize(ner2_tfidf)
        dot_product = sum([ner1_tfidf[w] * ner2_tfidf[w] for w in ner1_tfidf if w in ner2_tfidf])
        jaccardDistance = dot_product / (2.0 - dot_product)
        if jaccardDistance > duplicateThreshold:
            return -1.0
        return jaccardDistance

    @staticmethod
    def soergelsDistance(ner1, ner2, duplicateThreshold=0.2):
        """ sum(Ai -Bi, for all common i's) + Aj's + Bk's divided by
            sum(max(A,B))
        """
        if ner1 or ner2:
            ner1_tfidf = dict(map(calculate_tfidf, ner1.iteritems()))
            ner2_tfidf = dict(map(calculate_tfidf, ner2.iteritems()))
            common = [(ner1_tfidf[k], ner2_tfidf[k]) for k in ner1_tfidf if k in ner2_tfidf]
            common_diff = [abs(k[0] - k[1]) for k in common]
            common_max = [max(k) for k in common]
            ner2_unique = [ner2_tfidf[k] for k in ner2_tfidf if k not in ner1_tfidf]
            ner1_unique = [ner1_tfidf[k] for k in ner1_tfidf if k not in ner2_tfidf]
            return sum(common_diff + ner2_unique + ner1_unique) / sum(ner1_unique + ner2_unique + common_max)
        else:
            return 1.0


class updateHandler(object):
    def updateIDFTables(self):
        from datetime import datetime
        idftokens = {}
        idf = {'maxIdf': 0.0, 'tokens': idftokens}
        docCount = DOC_FREQ['docCount']
        tokens = DOC_FREQ['tokens']
        maxIdf = 0.0
        for word in tokens:
            idfWord = logarithm(docCount / (1 + tokens[word]))
            if idfWord > maxIdf:
                maxIdf = idfWord
            idftokens[word] = idfWord
        idf['maxIdf'] = maxIdf
        with codecs.open("%s/idf_%s" % (IDF_FOLDER, datetime.now().isoformat('T')), 'w', encoding='utf-8') as f:
            f.write(json.dumps(idf, encoding='utf-8'))
        with codecs.open("%s/docFreq_%s" % (DOC_FREQ_FOLDER, datetime.now().isoformat('T')), 'w', encoding='utf-8') as f:
            f.write(json.dumps(DOC_FREQ, encoding='utf-8'))
        return None

    def updateDocFreqCounts(self, freqVector):
        tokens = DOC_FREQ['tokens']
        DOC_FREQ['docCount'] += 1.0
        for k in freqVector:
            if k in tokens:
                tokens[k] += 1.0
            else:
                tokens[k] = 1.0
        return


class minHeap(object):
    """stores values greater than a threshold, has a min bound.
        Min Element is thrown out, when a better one is found.
    """
    def __init__(self, size, threshold):
        self.size = size
        self.eMap = {}  # {distance: entry}
        self.propertyMap = {}
        self.threshold = threshold
        self.top = 1

    def append(self, key, distance, prop=None):
        if distance < self.threshold:
            return
        if distance in self.eMap:
            self.eMap[distance].append(key)
            self.propertyMap[key] = prop
            return
        if len(self.eMap) < self.size:
            self.eMap[distance] = [key]
            self.propertyMap[key] = prop
            if distance < self.top:
                self.top = distance
            return
        if distance < self.top:
            return
        if distance > self.top:
            for k in self.eMap[self.top]:
                del self.propertyMap[k]
            del self.eMap[self.top]
            self.eMap[distance] = [key]
            self.propertyMap[key] = prop
            self.top = min(self.eMap)

    def __iter__(self):
        for distance in self.eMap:
            for key in self.eMap[distance]:
                yield key, distance, self.propertyMap[key]

    def isEmpty(self):
        return False if self.eMap else True


class maxHeap(object):
    """Stores values less  than a threshold...has a max bound.
       Max element is thrown out in case of pop
    """
    def __init__(self, size, threshold):
        self.size = size
        self.eMap = {}  # {distance: entry}
        self.propertyMap = {}
        self.threshold = threshold
        self.top = 0

    def append(self, key, distance, prop=None):
        if distance > self.threshold:
            return
        if distance in self.eMap:
            self.eMap[distance].append(key)
            self.propertyMap[key] = prop
            return
        if len(self.eMap) < self.size:
            self.eMap[distance] = [key]
            self.propertyMap[key] = prop
            if self.top < distance:
                self.top = distance
            return
        if distance > self.top:
            return
        if distance < self.top:
            for k in self.eMap[self.top]:
                del self.propertyMap[k]
            del self.eMap[self.top]
            self.eMap[distance] = [key]
            self.propertyMap[key] = prop
            self.top = max(self.eMap)

    def __iter__(self):
        for distance in self.eMap:
            for key in self.eMap[distance]:
                yield key, distance, self.propertyMap[key]

    def isEmpty(self):
        return False if self.eMap else True
