doNotRead = 0;
if doNotRead == 0,
    clear;
    SerialNumberX = '2012-78-01-15-tst';
    outputname = [SerialNumberX '.txt'];
    inputFileName1stPart = 'db_mx_p';
    winD = 5;
    fmin = 0.75;
    noSingleChain = 1;
    randomSelection = 0.50;
    numberOfPartitiones = 14;
    % data import
    FileList = {};
    fidx = 0;
    sosmall = 0;
    skiprandom = 0;
    for numP=1:numberOfPartitiones,
        ['Data Import' ' - ' int2str(numP)]
        inputFileName = [inputFileName1stPart int2str(numP) '.txt'];
        fid = fopen(inputFileName);
        while 1
            tline = fgetl(fid);
            if ~ischar(tline),   break,   end
        %     disp(tline)
            if rand<randomSelection,
                firstColon = strfind(tline,':');
                chainID = tline(4:firstColon(1)-2);
                CuBracketsOpen = strfind(tline,'{');
                CuBracketsClose = strfind(tline,'}');
                Date = tline(CuBracketsOpen(2)+3:CuBracketsOpen(2)+12);
                CuBracketsOpen = CuBracketsOpen(3:end);
                CuBracketsClose = CuBracketsClose(1:end-2);
                DateList = {};
                j = 0;
                for i=1:length(CuBracketsOpen),
                    ListOfWords = tline(CuBracketsOpen(i)+1:CuBracketsClose(i)-1);
                    Colons = strfind(ListOfWords,':');
                    WordList = {};
                    k = 0;
                    while ~isempty(Colons),
                        Word = ListOfWords(3:Colons(1)-2);
                        Comma = strfind(ListOfWords,',');
                        if ~isempty(Comma),
                            Count = str2num(ListOfWords(Colons(1)+1:Comma(1)-1));
                            ListOfWords = ListOfWords(Comma(1)+2:end);
                        else
                            Count = str2num(ListOfWords(Colons(1)+1:end));
                            ListOfWords = '';
                        end
                        k = k + 1;
                        WordList{k,1} = Word;
                        WordList{k,2} = Count;
                        Colons = strfind(ListOfWords,':');
                    end
                    j = j + 1;
                    DateList{j,1} = Date;
                    DateList{j,2} = WordList;
                    DoubleCota = strfind(tline(CuBracketsClose(i)+1:end),'u');
                    if ~isempty(DoubleCota),
                        Date = tline(CuBracketsClose(i)+DoubleCota(1)+2:CuBracketsClose(i)+DoubleCota(1)+11);
                    else
        %                 'Last Date of Record processed'
                        break;
                    end
                end
                if noSingleChain==0 || j>1,
                    fidx = fidx + 1;
                    FileList{fidx,1} = DateList;
                    FileList{fidx,2} = chainID;
                else
                    sosmall = sosmall + 1;
                end
            else
                skiprandom = skiprandom + 1;
            end
        end
        fclose(fid);
    end
%     break;
% else
% %     clear;
%     SerialNumberX = '2012-78-01-15-tst';
%     outputname = [SerialNumberX '.txt'];
%     inputFileName1stPart = 'db_mx_p';
%     winD = 5;
%     fmin = 0.95;
%     noSingleChain = 1;
%     randomSelection = 0.02;
%     numberOfPartitiones = 14;
%     load file_list_09percent_6weeks.mat
end
% word list
'Word List'
listOfAllWords = {};
SetOfChains = {};
flS = size(FileList);
mindate = 2020*365+31+12*30;
for i=1:flS(1),
    Chains = [];
    Times = [];
    DateList = FileList{i,1};
    dlS = size(DateList);
    for j=1:dlS(1),
        Date = DateList{j,1};
        dash = strfind(Date,'-');
        y = str2num(Date(1:dash(1)-1));
        m = str2num(Date(dash(1)+1:dash(2)-1));
        d = str2num(Date(dash(2)+1:end));
        DateStamp = y*365+m*31+d;
        if DateStamp<mindate,
            mindate = DateStamp;
        end
        WordIDs = [];
        WordList = DateList{j,2};
        wlS = size(WordList);
        for k=1:wlS(1),
            w1 = WordList{k,1};
            found = 0;
            L = size(listOfAllWords,1);
            for l=1:L,
                w2 = listOfAllWords{l,1};
                if strcmp(w1,w2)==1,
                    found = l;
                    break;
                end
            end
            if found==0,
                listOfAllWords{L+1,1} = w1;
                found = L+1;
            end
            WordIDs(k,1) = found;
        end
        Times(1,end+1) = DateStamp;
        s = size(Chains);
        if s(1)==0,
            Chains = WordIDs;
        else
            if s(1)>length(WordIDs),
                zzz = zeros(s(1)-length(WordIDs),1);
                WordIDs = [WordIDs ; zzz];
                Chains = [Chains WordIDs];
            else
                if s(1)<length(WordIDs),
                    zzz = zeros(length(WordIDs)-s(1),s(2));
                    Chains = [Chains ; zzz];
                    Chains = [Chains WordIDs];
                else
                    Chains = [Chains WordIDs];
                end
            end
        end
    end
    SetOfChains{i,1} = Times;
    SetOfChains{i,2} = Chains;
    SetOfChains{i,3} = FileList{i,2};
end
% Re-Timing (Current-time (minus) minimum-time (plus) 1)
'Time Shift'
scS = size(SetOfChains);
mindate = mindate - 1;
for i=1:scS(1),
    s = size(SetOfChains{i,1});
    for j=1:s(2),
        SetOfChains{i,1}(1,j) = SetOfChains{i,1}(1,j) - mindate;
    end
end
% Mining
'Mining'
scS = size(SetOfChains);
winD = 0;
for idx=1:scS(1),
    chain = SetOfChains{idx,2};
    if size(chain,2)>winD,
        winD = size(chain,2);
    end
    % injection
    newchain = chain(:,1);
    s = size(chain);
    s1 = size(newchain,1);
    for i=2:s(2),
        foradd = [];
        for j=1:s1,
            if newchain(j,i-1)~=0,
                if isempty(find(newchain(j,i-1)==chain(:,i))),
                    foradd(end+1,1) = newchain(j,i-1);
                end
            end
        end
        foradd = [foradd ; chain(:,i)];
        sx = size(newchain);
        if sx(1)>length(foradd),
            zzz = zeros(sx(1)-length(foradd),1);
            foradd = [foradd ; zzz];
            newchain = [newchain foradd];
        else
            if sx(1)<length(foradd),
                zzz = zeros(length(foradd)-sx(1),sx(2));
                newchain = [newchain ; zzz];
                newchain = [newchain foradd];
            else
                newchain = [newchain foradd];
            end
        end
        s1 = size(newchain,1);
    end
    chain = newchain;
    SetOfChains{idx,2} = chain;
end
scS = size(SetOfChains);
N = scS(1);
ResultTables = {};
allevents = length(listOfAllWords);
for i=1:allevents,
    ResultTables{i,1} = i;
    ResultTables{i,2} = 0;
    ResultTables{i,3} = 0;
end
FirstRound = 1;
goodevents = [];
for winIdx=1:winD,
    for chainIdx=1:scS(1),
        chain = SetOfChains{chainIdx,2};
        if winIdx<=size(chain,2),
            times =  1:size(chain,2);
            [chainIdx winIdx winD]
            te = max(times);
            s = size(ResultTables);
            for i=1:s(1),
                if ResultTables{i,3}==0,
                    [chainIdx winIdx winD 0 i s(1)]
                    temp = ResultTables{i,1};
                    if existanceVector(temp,chain,times,1,te)==1,
                        c = ResultTables{i,2};
                        c = c + 1;
                        ResultTables{i,2} = c;
                    end
                end
            end
        end
    end
    i = 1;
    pruneStart = 0;
    while i<=s(1),
        [chainIdx winIdx winD 0 i s(1)]
        if ResultTables{i,3}<2,
            c = ResultTables{i,2};
            if (c/N)<fmin,
                if i==1,
                    ResultTables = ResultTables(2:end,:);
                else
                    if i==s(1),
                        if pruneStart==0,
                            ResultTables = ResultTables(1:end-1,:);
                        else
                            i = i + 1;
                        end
                    else
                        if pruneStart==0,
                            pruneStart = i;
                        end
                        i = i + 1;
                    end
                end
            else
                if pruneStart~=0,
                    ResultTables = [ResultTables(1:pruneStart-1,:) ; ResultTables(i:end,:)];
                    i = pruneStart;
                    pruneStart = 0;
                end
                ResultTables{i,3} = 1;
                if FirstRound==1,
                    goodevents(end+1) = ResultTables{i,1};
                end
                i = i + 1;
            end
            s = size(ResultTables);
        else
            i = i + 1;
        end
    end
    if pruneStart~=0,
        ResultTables = ResultTables(1:pruneStart-1,:);
        pruneStart = 0;
    end
    FirstRound = 0;
    if winIdx<winD
        s = size(ResultTables);
        for i=1:s(1),
            [chainIdx winIdx winD 0 i s(1)]
            if ResultTables{i,3}==1,
                ResultTables{i,3} = 2;
                temp = ResultTables{i,1};
                for j=1:length(goodevents),
                    if isempty(find(temp==goodevents(j))),
                        ResultTables{end+1,1} = [temp goodevents(j)];
                        ResultTables{end,2} = 0;
                        ResultTables{end,3} = 0;
                    end
                end
            end
        end
    end
end
'Finalizing'
i = 1;
s = size(ResultTables);
pruneStart = 0;
while i<=s(1),
%         [chainIdx iter win 0 i s(1)]
    c = ResultTables{i,2};
    if (c/N)<fmin,
        if i==1,
            ResultTables = ResultTables(2:end,:);
        else
            if i==s(1),
                if pruneStart==0,
                    ResultTables = ResultTables(1:end-1,:);
                else
                    i = i + 1;
                end
            else
                if pruneStart==0,
                    pruneStart = i;
                end
                i = i + 1;
            end
        end
    else
        if pruneStart~=0,
            ResultTables = [ResultTables(1:pruneStart-1,:) ; ResultTables(i:end,:)];
            i = pruneStart;
            pruneStart = 0;
        end
        ResultTables{i,3} = 1;
        i = i + 1;
    end
    s = size(ResultTables);
end
if pruneStart~=0,
    ResultTables = ResultTables(1:pruneStart-1,:);
    pruneStart = 0;
end

% ----------------------------------------------------------------------

newTable = {};
s = size(ResultTables);
k = 0;
'Pruning ...'
for i=1:s(1),
    c1 = ResultTables{i,1};
    found = 0;
    for j=1:s(1),
        c2 = ResultTables{j,1};
        if length(c1)<length(c2),
            if existanceNoTime(c1,c2)==1,
                found = 1;
                break;
            end
        end
    end
    if found==0,
        k = k + 1;
        newTable{k,1} = ResultTables{i,1};
        newTable{k,2} = ResultTables{i,2};
        newTable{k,3} = ResultTables{i,3};
    end
end
s = size(newTable);
ss = size(SetOfChains,1);
'List of Corresponding Chains ...'
for i=1:s(1),
    if i==5,
        'hum'
    end
    c1 = newTable{i,1};
    chainIDs = {};
    for j=1:ss,
        c2 = SetOfChains{j,2};
        times =  1:size(c2,2);
        te = max(times);
        if existanceVector(c1,c2,times,1,te)==1,
            chainIDs{end+1,1} = SetOfChains{j,3};
        end
    end
    newTable{i,5} = chainIDs;
end
'Sorting and Code to Word ...'
for i=1:s(1)-1,
    jjj = i + 1;
    while jjj<=s(1),
        a = newTable{i,2};
        b = newTable{jjj,2};
        if a<b,
            temp1 = newTable{i,1};
            temp2 = newTable{i,2};
            temp3 = newTable{i,3};
            newTable{i,1} = newTable{jjj,1};
            newTable{i,2} = newTable{jjj,2};
            newTable{i,3} = newTable{jjj,3};
            newTable{jjj,1} = temp1;
            newTable{jjj,2} = temp2;
            newTable{jjj,3} = temp3;
        end
        jjj = jjj + 1;
    end
    words = {};
    c = newTable{i,1};
    for j=1:length(c),
        words{1,j} = listOfAllWords{c(j),1};
    end
    newTable{i,4} = words;
end
i = s(1);
words = {};
c = newTable{i,1};
for j=1:length(c),
    words{1,j} = listOfAllWords{c(j),1};
end
newTable{i,4} = words;
fid = fopen(outputname, 'w');
for i=1:s(1),
    fprintf(fid, '%d \t ', newTable{i,2});
    words = newTable{i,4};
    for j=1:size(words,2)-1,
        fprintf(fid, '%s -------> ', words{1,j});
    end
    fprintf(fid, '%s\n', words{1,end});
end
fclose(fid)
fid = fopen(['JSON' outputname], 'w');
for i=1:s(1),
    Ystr = int2str(i*1000+round(rand*999));
    while length(Ystr)<8,
        Ystr = ['0' Ystr];
    end
    SerialNumberY = [SerialNumberX '-' Ystr];
    fprintf(fid, '{"episodeid": "%s", "freq": %d, "episode": [', SerialNumberY, newTable{i,2});
    words = newTable{i,4};
    for j=1:size(words,2)-1,
        fprintf(fid, '"%s", ', words{1,j});
    end
    fprintf(fid, '"%s"], "chainids": [', words{1,end});
    chainids = newTable{i,5};
    for j=1:size(chainids,1)-1,
        fprintf(fid, '"%s", ', chainids{j,1});
    end
    fprintf(fid, '"%s"]}\n', chainids{end,1});
end
fclose(fid);
