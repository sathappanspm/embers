import json
import chardet
from embersUtils.BasisServiceClient import BasisEnricher
import sys


def main(f):
    kd = {'spanish': [], 'english': [], 'portuguese': []}
    with open(f) as keywords:
        lang = ['spanish', 'english', 'portuguese']
        for l in keywords:
            ld = l.decode(chardet.detect(l)['encoding'])
            i = 0
            for w in ld.split(','):
                if w.strip():
                    kd[lang[i]].append(w.strip())
                i += 1

    esWords = ' '.join(kd['spanish'])
    enWords = ' '.join(kd['english'])
    ptWords = ' '.join(kd['portuguese'])
    bs = BasisEnricher()
    rs = bs.requestJson({'text': esWords})
    print rs['BasisEnrichment']['errorMessage']
    kd['spanish'] = [k['lemma'] for k in rs['BasisEnrichment']['tokens'] if k['value'] in kd['spanish']]

    ren = bs.requestJson({'text': enWords})
    print ren['BasisEnrichment']['errorMessage']
    kd['english'] = [k['lemma'] for k in ren['BasisEnrichment']['tokens'] if k['value'] in kd['english']]

    rpt = bs.requestJson({'text': ptWords})
    print rpt['BasisEnrichment']['errorMessage']
    kd['portuguese'] = [k['lemma'] for k in rpt['BasisEnrichment']['tokens'] if k['value'] in kd['portuguese']]
    return kd


if __name__ == "__main__":
    kd = main(sys.argv[1])
    print len(kd['spanish'])
    out = open('keyword_dict.txt', 'w')
    out.write(json.dumps(kd, encoding='utf-8'))
    out.close()
