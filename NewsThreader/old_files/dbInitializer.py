#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    dbInitialize.py: This File creates the required Keyspaces and Column Families.
    Last Modified:Sun Dec 30 23:21:37 2012
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

from pycassa.system_manager import *

sys = SystemManager('localhost:9160')
sys.create_keyspace('embers', SIMPLE_STRATEGYh, {'replication_factor': '1'})
sys.create_column_family('embers', 'article', super=true, comparator_type=UTF8_TYPE)



