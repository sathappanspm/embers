#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
    Last Modified:Tue Jan 22 17:47:45 2013
"""
from datetime import timedelta
from utils import COUNTRY_REVERSE_CODES, isodate, Similarity, minHeap, maxHeap
from dbHandler import columnFamily, dbConnect
import uuid
from math import log as logarithm
from etool import logs

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = "chainer"
TIME_DELTA = timedelta(days=0)
CHAIN_NUM = 0
log = logs.getLogger(__processor__)
minSimThreshold = 0.35
minCombinedDistanceThreshold = 1.90
duplicateThreshold = 0.8
minChainThreshold = 0.8
minLastEleChainThreshold = 0.2
simFile = open('SimilarityCheckFile.txt', 'w')

class chainer(object):
    def __init__(self, n, CONFIG_DIR, buffer_size):
        self.lookBackDays = n
        self.tDelta = timedelta(days=n)
        self.dbCon = dbConnect('embers')
        self.articleDb = columnFamily(self.dbCon, 'Articles', buffer_size, ['publishedDate'])
        self.chainDb = columnFamily(self.dbCon, 'Chains', buffer_size, ['lastUpdateTime'])
        self.entityDb = columnFamily(self.dbCon, 'Persons', buffer_size)
        self.locationsDb = columnFamily(self.dbCon, 'Locations', buffer_size)

    def close(self):
        self.articleDb.flush()
        self.chainDb.flush()
        self.dbCon.close()

    def chain(self, article):
        """Obtain last n days articles of the given country and pick the top
           5 most articles above a given threshold and chain"""
        article['publishedDate'] = isodate(article['publishedDate'])
        topFiveSimilar = self.getTopFive(article)
        if topFiveSimilar is False:
            return False
        article['chainIds'] = []
        simFile.write("==%s   %s ==" % (article['url'], article['embersId']))
        for csKey, csDistance, csProp in topFiveSimilar:
            simFile.write("\n%s   %s   %s" %(csProp['url'], csDistance, csKey))
        simFile.write("**************\n\n\n")
        #articleUpdatebatchObj = {}
        for cKey, cProp in self.linkToChain(article, topFiveSimilar):
            for eleKey, eleProp in self.articleDb.getMany(cProp['elements']):
                eleProp['chainIds'].append(cKey)
                #articleUpdatebatchObj[eleKey] = eleProp
                self.insertArticle(eleProp, eleKey)
            self.insertChain(cKey, cProp, article)
            article['chainIds'].append(cKey)
        #if articleUpdatebatchObj:
        #    self.articleDb.batch_insert(articleUpdatebatchObj)
        self.insertArticle(article, article['embersId'])
        return True

    def getLastN(self, article):
        cCode = COUNTRY_REVERSE_CODES[article['country']]
        endsWith = '_'.join([cCode, (article['publishedDate'] + timedelta(days=1)).strftime('%Y-%m-%d')])  # 'Z' is added in order to include endswith also.
        startsWith = '_'.join([cCode, str(article['publishedDate'] - self.tDelta)])
        entKeysIter = self.entityDb.getMany(article["entities"].keys(), startsWith, endsWith)
        locKeysIter = self.locationsDb.getMany(article["locations"].keys(), startsWith, endsWith)
        keySet = set()
        [keySet.update(v.keys()) for k, v in entKeysIter]
        [keySet.update(v.keys()) for k, v in locKeysIter]
        return self.articleDb.getMany(list(keySet))
        #return self.articleDb.getRange(startsWith, endsWith)
        #return self.articleDb.getByClause('mexico', publishedDate.isoformat('T'))

    def getCombinedDistance(self, doc1, doc2, cosineDistance, threshold):
        """ returns distance: lesser the value more the similarity"""
        locDistance = Similarity.soergelsDistance(doc1['locations'], doc2['locations'])
        entDistance = Similarity.soergelsDistance(doc1['entities'], doc2['entities'])
        total = locDistance + entDistance
        if not locDistance:
            return False
        if total > threshold:
            return False
        combinedDistance = 0.6 * (1 - cosineDistance) + 0.3 * locDistance + 0.1 * entDistance
        return combinedDistance

    def insertArticle(self, article, articleKey):
        self.articleDb.insert(articleKey, article)
        entity_batchObj = {k: {article['embersId']: ''} for k in article['entities']}
        loc_batchObj = {k: {article['embersId']: ''} for k in article['locations']}
        self.locationsDb.batch_insert(loc_batchObj)
        self.entityDb.batch_insert(entity_batchObj)
        return articleKey

    def insertChain(self, chainKey, chain, article):
        if 'protest' in article:
            chain['protest'] = True
        chain['lastUpdateTime'] = article['publishedDate']
        chain['elements'].append(article['embersId'])
        self.chainDb.insert(chainKey, chain)
        return None

    def getTopFive(self, article):
        articleIter = self.getLastN(article)
        mostSimilar = minHeap(10, minSimThreshold)
        for key, value in articleIter:
            distance = Similarity.cosineSimilarity(article['freqVector'], value['freqVector'], duplicateThreshold)
            if distance < 0:
                if 'protest' in article:
                    value['protest'] = True
                    self.insertArticle(value, value['embersId'])
                log.info('article %s is a duplicate of url: %s   embersId:%s' % (article['url'],
                                                                                 value['url'], value['embersId']))
                #-1 indicates the article is a duplicate
                return False
            mostSimilar.append(key, distance, value)
        topFive = maxHeap(5, minCombinedDistanceThreshold)
        for k, d, v in mostSimilar:
            distance = self.getCombinedDistance(article, v, d, threshold=1.9)
            if distance:
                topFive.append(k, distance, v)
        return topFive

    def linkToChain(self, article, topFive):
        topChains = maxHeap(3, minChainThreshold)
        chainsSeen = set()
        if topFive.isEmpty():
            chainKey, chainProperties = self.createNewChain(article)
            yield chainKey, chainProperties
        else:
            for articleKey, cs, articleProp in topFive:
                chainIds = articleProp['chainIds']
                chains = {c: self.getChainSimilarity(article, c)
                          for c in chainIds if c not in chainsSeen}
                chainsSeen.update(chainIds)
                for key in chains:
                    if chains[key][0] > 0:
                        topChains.append(key, chains[key][0], chains[key][1])
                    else:
                        log.info('Distance with last element did not pass the threshold for chain %s' % key)
            for cKey, cDistance, cProp in topChains:
                self.updateChain(article, cProp)
                cProp['chainSize'] += 1
                cProp['letters']['_'.join([str(article['publishedDate']), str(cProp['chainSize'])])] = article['letters']
                log.info('article added to chain %s with distance %s' % (cKey, cDistance))
                yield cKey, cProp
            if topChains.isEmpty():
                branch_iter = self.createBranchStory(article, topFive)
                for branchKey, branchProp in branch_iter:
                    log.info('branch chain added with key %s for article %s' % (branchKey, article['embersId']))
                    yield branchKey, branchProp
                #TODO: replace the above for loop with below line after testing
                #yield self.createBranchStory(article, topFive)

    def createBranchStory(self, article, topFive):
        chainSeen = set()
        for k, cs, kProp in topFive:
            branchKey, branchChain = self.createNewChain(article, k)
            chainIds = {c: self.chainDb.get(c, ['elements'])[1]['elements'] for c in
                        kProp['chainIds'] if c not in chainSeen}
           # articleSeen = set([k])
            self.updateChain(kProp, branchChain)
            prevNode = article
            branchChain['elements'].append(k)
            uniqueArticles = [k]
            [uniqueArticles.extend(v) for k, v in chainIds.iteritems()]
            uniqueArticles = set(uniqueArticles)
            for elemKey, elemProp in self.articleDb.getMany(list(uniqueArticles)):
                if (article['publishedDate'] - elemProp['publishedDate']).days < 60:
                    distance = Similarity.cosineSimilarity(article['freqVector'],
                                                           elemProp['freqVector'], 0.8)
                    if distance > 0.25:
                        sum = self.getCombinedDistance(article, elemProp, distance, 1.9)
                        if sum < 0.9:
                            prevSimilarity = Similarity.cosineSimilarity(elemProp['freqVector'],
                                                                         prevNode['freqVector'], 0.8)
                            if prevSimilarity > 0.25:
                                prevNode = elemProp
                                self.updateChain(elemProp, branchChain)
                                branchChain['elements'].append(elemKey)

            branchChain['elements'].sort()
            yield branchKey, branchChain

    def getChainSimilarity(self, article, cID):
        try:
            cProp = self.chainDb.get(cID)[1]
        except Exception:
            log.warning("db manual Flush ---point 1")
            self.chainDb.flush()
            self.articleDb.flush()
            cProp = self.chainDb.get(cID)[1]
        try:
            lastElement = self.articleDb.get(cProp['elements'][-1])[1]
        except Exception:
            log.warning("db manual flush point 2")
            self.chainDb.flush()
            self.articleDb.flush()
            lastElement = self.articleDb.get(cProp['elements'][-1])[1]
        distanceWithLastElement = Similarity.cosineSimilarity(article['freqVector'], lastElement['freqVector'])
        if distanceWithLastElement < minLastEleChainThreshold:
            #TODO: if distance with lastElement is less than the min threshold, return None
            return (-1.0, None)
        entDistance = Similarity.soergelsDistance(cProp['entities'], article['entities'])
        locDistance = Similarity.soergelsDistance(cProp['locations'], article['locations'])
        chainSimilarity = 0.4 * entDistance + 0.6 * locDistance
        return (chainSimilarity, cProp)

    def updateChain(self, article, cProp):
        #chainUpdates:  if the article contains too many items the article is not considered for update
        if len(article['entities']) < 15:
            for item in cProp['entities']:
                if not item in article['entities']:
                    cProp['entities'][item] -= self.getTimeDecay(article['publishedDate'], cProp['lastUpdateTime'])
                    if cProp['entities'][item] < 0.0:
                        cProp['entities'][item] = 0.0
            for item in article['entities']:
                if not item in cProp['entities']:
                    cProp['entities'][item] = article['entities'][item]
                else:
                    cProp['entities'][item] += article['entities'][item]
        if len(article['locations']) < 10:
            for item in cProp['locations']:
                if not item in article['locations']:
                    timeDecay = self.getTimeDecay(article['publishedDate'], cProp['lastUpdateTime'])
                    cProp['locations'][item] -= timeDecay
                    if cProp['locations'][item] < 0.0:
                        cProp['locations'][item] = 0.0
            for item in article['locations']:
                if not item in cProp['locations']:
                    cProp['locations'][item] = article['locations'][item]
                else:
                    cProp['locations'][item] += article['locations'][item]
        cProp['letters']['_'.join([article['publishedDate'].strftime('%Y-%m-%d'),
                                   str(cProp['chainSize'] + 1)])] = article['letters']
        cProp['chainSize'] += 1
        return None

    def createNewChain(self, article, doc2Key=''):
        global CHAIN_NUM
        CHAIN_NUM += 1
        dtStr = article['publishedDate'].strftime('%Y-%m-%d')
        cKey = str(uuid.uuid5(uuid.NAMESPACE_DNS, ('_'.join([dtStr, article['embersId'], article['url'],
                                                             doc2Key])).encode('utf-8')))
        lettersKey = '_'.join([dtStr, '1'])
        cProp = {'elements': [], 'locations': article['locations'], 'embersId': cKey, 'lastUpdateTime': article['publishedDate'],
                 'entities': article['entities'], 'letters': {lettersKey: article['letters']}, 'chainSize': 1}
        log.info('new chain %s created %s' % (cKey, CHAIN_NUM))
        return cKey, cProp

    def getTimeDecay(self, artDate, chainUpdatedDate, delta=7):
        tDelta = (artDate - chainUpdatedDate).days
        noOfPeriods = tDelta / delta
        if noOfPeriods < 0.0:
            noOfPeriods = -noOfPeriods
        decay = logarithm(noOfPeriods + 1)
        if decay < 0.0:
            decay = -decay
        return decay
