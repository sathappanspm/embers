#-*- coding:utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
import urllib2
import socket
import httplib
import sys
import json
from etool import logs, args
import traceback
import chardet
import codecs

__processor__ = 'feed_content_ingest.py'
log = logs.getLogger('%s.log' % (__processor__))


def getContent(html_content, Extractor):
    try:
        content = Extractor(extractor='ArticleExtractor', html=html_content).getText()
    except Exception, e:
        log.error("content Extractor Error: %s" % str(e))
        return ''
    if content == '':
        try:
            content = Extractor(extractor='LargestContentExtractor', html=html_content).getText()
        except Exception, e:
            log.error("content Extractor error: %s" % str(e))
            return ''
        if content == '':
            try:
                content = Extractor(extractor='DefaultExtractor', html=html_content).getText()
            except Exception, e:
                log.error('content Extractor error: %s' % str(e))
                return ''
    return content


def getHTML(link):
    try:
        urlHandler = urllib2.urlopen(link, timeout=10)
        finalUrl = urlHandler.geturl()
        html_content = urlHandler.read()
        html_content = html_content.decode(chardet.detect(html_content)['encoding'])
    except urllib2.HTTPError:
        log.error("urllib2 HTTP error: %s" % link)
        return None, None
    except UnicodeDecodeError:
        log.error("utf8 codec error: %s" % link)
        return None, None
    except socket.timeout:
        log.error("socket timeout: %s" % link)
        return None, None
    except httplib.BadStatusLine:
        log.error('httplib BadStatusLine : %s' % link)
        return None, None
    except urllib2.URLError:
        log.error("urllib2 URLError: %s" % link)
        return None, None
    except Exception, e:
        log.error('unknown Exception: %s ' % str(e))

    if html_content is None:
        return None, finalUrl
    return html_content, finalUrl


def main():
    parser = args.get_parser()
    parser.add_argument('jsonFile', type=str, help='path to the feedsFile')
    parser.add_argument('-o', '--outFile', type=str, default='sys.stdout', help='output file')
    parser.add_argument('-l', '--lib', default='/usr/lib/jvm/java-7-openjdk/jre', type=str, help='Path to jre')
    parser.add_argument('-d', '--download', action="store_true", default=False,
                        help='Flag to initiate downloading urls')
    arg = parser.parse_args()

    import os
    os.environ['JAVA_HOME'] = arg.lib
    from boilerpipe.extract import Extractor as Extractor

    jdumps = json.dumps
    logs.init(arg)
    jload = json.loads
    if arg.outFile == 'sys.stdout':
        writer = codecs.getwriter('utf-8')(sys.stdout)
    else:
        writer = codecs.open(arg.outFile, 'w', encoding='utf-8')
    write = writer.write
    with codecs.open(arg.jsonFile, 'r', encoding='utf-8') as f:
        for l in f:
            try:
                json_data = jload(l.strip())
                if arg.download:
                    url_link = json_data['url']
                    html_content, finalUrl = getHTML(url_link)
                    if html_content is not None:
                        json_data['boilerpipe_content'] = getContent(html_content, Extractor)
                    else:
                        json_data['boilerpipe_content'] = ''
                    json_data['finalUrl'] = finalUrl
                    json_data['content'] = html_content
                else:
                    json_data['boilerpipe-content'] = getContent(json_data['content'])
                #json_data['boilerpipe_content'] = content
                write(jdumps(json_data))
                write('\n')
            except:
                log.critical('Unable to process json: %s' % traceback.print_exc())
                write(l)
                write('\n')

    if arg.outFile != 'sys.stdout':
        writer.close()
if __name__ == '__main__':
    main()
