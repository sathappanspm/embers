# -*- coding:utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4

import uuid
from datetime import datetime
from etool import logs
import json

__processor__ = "warningGen"
__version__ = '1.0.3.v1'
log = logs.getLogger(__processor__ + "%s.log" % str(datetime.now()))


SELECTED_CO = set(["argentina", "brazil", "chile", "colombia", "ecuador", "el salvador", "mexico", "paraguay",
                  "uruguay", "venezuela"])
OSI_FORMAT_LOC_CORRECT = {'Mexico,Distrito Federal,Ciudad De Mexico': 'Mexico,Ciudad de M�xico,Ciudad de M�xico',
                          'Mexico,Distrito Federal,': 'Mexico,Ciudad de Mexico,Ciudad de Mexico',
                          'Argentina,Distrito Federal,Buenos Aires': 'Argentina,,Buenos Aires',
                          'Venezuela,Distrito Capital,Caracas': 'Venezuela,Caracas,Caracas',
                          'Venezuela,Distrito Capital,': 'Venezuela,Caracas,Caracas',
                          'Argentina,Distrito Federal,': 'Argentina,,Buenos Aires'}


class warning ():
    def __init__(self, model_name):
        self.warning = {}
        self.conf_prob_flag_lbl = "confidenceIsProbability"
        self.warning[self.conf_prob_flag_lbl] = "True"
        self.date_label = "date"
        self.event_date_lbl = "eventDate"
        self.population_lbl = "population"
        self.confidence_lbl = 'confidence'
        self.model_lbl = 'model'
        self.id_lbl = 'embersId'
        self.derivedFrom_lbl = 'derivedFrom'
        self.location_lbl = 'location'
        self.warning[self.model_lbl] = '%s-%s' % (model_name, __version__)
        self.conf_penalty = 0.0
        self.eventType_lbl = 'eventType'
        self.comment_lbl = 'comments'

    def send(self, zmqueue, publishedDate, sysout, url):
        self.warning[self.id_lbl] = self.generateId()
        self.warning[self.date_label] = publishedDate
        self.warning[self.comment_lbl] = "PlannedProtest-%s url: %s" % (__version__, url)
        if self.warning[self.location_lbl][0].lower() in SELECTED_CO:
            if sysout:
                zmqueue.write(json.dumps(self.warning, encoding='utf-8'))
                zmqueue.write('\n')
            else:
                zmqueue.write(self.warning)
        #log.info(json.dumps(self.warning, encoding='utf-8'))
        return None

    def generateId(self):
        l = ''
        for k in self.warning[self.location_lbl]:
            l = l + k
        df = ''
        for k in self.warning[self.derivedFrom_lbl]:
            df = df + k
        text = datetime.utcnow().isoformat('T')
        uid = str(uuid.uuid5(uuid.NAMESPACE_DNS, text.encode('utf-8')))
        return uid

    def setEventDate(self, event_date):
        if event_date is None:
            self.warning[self.event_date_lbl] = ''
        self.warning[self.event_date_lbl] = event_date

    def setLocation(self, loc):
        """Sets location  in format city,state,country from an input of type 'city':{'country':'state'}"""
        locationStr = ','.join([loc[0].title(), loc[1].title(), loc[2].title()])
        if locationStr in OSI_FORMAT_LOC_CORRECT:
            locationStr = OSI_FORMAT_LOC_CORRECT[locationStr]
        self.warning[self.location_lbl] = locationStr.split(',')
        return

    def setPopulation(self, popln):
        if popln is None:
            self.warning[self.population_lbl] = ''
            return
        self.warning[self.population_lbl] = popln

    def setConfidence(self, conf):
        #if conf is None or conf == {}:
        self.warning[self.confidence_lbl] = conf
        #self.warning[self.confidence_lbl] = (sum(conf.values()) + self.conf_penalty) / len(conf)

    def setDerivedFrom(self, derived, source):
        if derived is None:
            self.warning[self.derivedFrom_lbl] = ''
        self.warning[self.derivedFrom_lbl] = {'derivedIds': [derived['embersId']],
                                              'start': derived['date'], 'end': derived['date'],
                                              'model': 'plannedProtest-%s' % __version__,
                                              'source': source}

    def sendSurrogate(self, surrogateWriter, timePhrase, derived, source, queryFile, url, sysout):
        self.surrogate = {}
        self.surrogate['articleUrl'] = url
        self.surrogate['derivedFrom'] = {'derivedIds': [derived['embersId']], 'resource': queryFile,
                                         'start': derived['date'], 'end': derived['date']}
        self.surrogate['derivedIds'] = derived['embersId']
        self.surrogate['location'] = self.warning[self.location_lbl]
        self.surrogate['model'] = 'PlannedProtest %s' % __version__
        self.surrogate['timePhrase'] = timePhrase
        self.surrogate['triggerPhrase'] = derived['keyPhrases']['phrasesPresent']['phrases']
        self.surrogate['source'] = source
        self.surrogate['embersId'] = str(uuid.uuid5(uuid.NAMESPACE_DNS,
                                         json.dumps(self.surrogate, encoding='utf-8')))
        if self.surrogate['location'][0].lower() in SELECTED_CO:
            if sysout:
                surrogateWriter.write(json.dumps(self.surrogate, encoding='utf-8'))
                surrogateWriter.write('\n')
            else:
                surrogateWriter.write(self.surrogate)
        log.info(json.dumps(self.surrogate, encoding='utf-8'))

    def setEventType(self, evtType):
        self.warning[self.eventType_lbl] = evtType
