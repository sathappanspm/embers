Planned Protest (RSS, Twitter-url, mailing-lists, google-news alerts)
========================
General Information
--------------------
This Module accepts sends out warnings from files that contain the planned protest phrases by analyzing the datesmentioned in the article and the locations in the article.

Geocoder.py:
-------------
    Geocoding of RSS articles
Usage :
---------
    python geocoder.py --sub "tcp://*.31220" -c config --pub "tcp://*.30115"

planned protest.py:
-------------------
    generate suitable warning based on geoCoding and phrasesPresent

Usage:
------
        usage: planned_protest.py [-h] [--pub PUBLISH [PUBLISH ...]]
                          [--sub SUBSCRIBE [SUBSCRIBE ...]]
                          [--queue-conf [QUEUE_CONF]] [--tunnel TUNNEL]
                          [--ssh-key KEYFILE] [--aws-key AWSKEY]
                          [--aws-secret AWSSECRET] [--verbose] [-f FEED]
                          [-c CONFIG] [-s SURROGATE] [-t SOURCETYPE] [-n] [-g]
                          [-p]

Example Run:
--------------
        #geoCoder produces surrogates, with geocoded information and publishes it onto a queue
        1. python geoCoder.py -c config --pub "tcp://*:30115" --sub "tcp://*"

        # the surrogate formed above is used by keyPhraseFinder
        2. python keyPhraseFinder.py --sub "tcp://*:30115" --pub "tcp://*:31225" -n -k "resources/keywords/plannedProtest" -b "embers-osi-data"

        #the output of keyPhraseFinder.pu is then used to produce warnings
        1. python python planned_protest.py --sub tcp://*:31225 --pub tcp://* -s tcp://*:30118 -n[if evt classification has to be turned off] -g [if only country info is required in the warning]
