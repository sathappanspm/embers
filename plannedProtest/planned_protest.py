#!/usr/bin/env python
# -*- coding:utf-8 -*-
# vim: tabstop=8 expandtab shiftwidth=4 softtabstop=4
'''%prog
Program to form warnings out of contents that pass the planned protest Filter(Filter.py).
To be run with output of Filter.py (with planned Protest phrase Dictionary) as input.

Config Folder: Contains a city_data.txt that is used for mapping locations to state and country.
'''

from etool import args, logs, queue
import json
from warning import warning
from datetime import datetime
from classification.supervised.naivebayes import CivilUnrestClassifierFactory
import codecs
import sys
import re

__processor__ = 'plannedProtest'
__version__ = '1.0.3v1'

cFactory = CivilUnrestClassifierFactory()
pTypeClassifier = cFactory.create_classifier(usecase='protest', top=200)
pNatureClassifier = cFactory.create_classifier(usecase='violence', top=200)
poplnClassifier = cFactory.create_classifier(usecase='population', top=200)
POPULATION_TYPE_CODES = {'01': "General Population", '02': "Business",
                         '03': "Ethnic", '04': "Legal", '05': "Education",
                         '06': "Religious", '07': "Medical", '08': "Media",
                         '09': "Labor", '10': "Refugees/Displaced",
                         '11': "Agricultural"
                         }
COUNTRY_CODES = {'ar': "argentina", 'bz': 'belize', 'bo': 'bolivia',
                 'cl': 'chile', 'co': 'colombia', 'cr': 'costa rica',
                 'ec': 'ecuador', 'sv': 'el salvador', 'gf': 'french guiana',
                 'gt': 'guatemala', 'gy': 'guyana', 'hn': 'honduras',
                 'ni': 'nicaragua', 'pa': 'panama', 'py': 'paraguay',
                 'pe': 'peru', 'sr': 'suriname', 'uy': 'uruguay',
                 've': 'venezuela', 'br': 'brazil', 'mx': "mexico"
                 }

classifier_flag = True
strictGeo = True
SYS_OUT = False
SYS_IN = False
dateRE = re.compile('\d\d\d\d-\d\d-\d\d', re.I)
log = logs.getLogger('%s-%s.log' % (__processor__, str(datetime.now())))


class dateHandler(object):
    def getDate(self, enrichedFeed, published_date):
        """A function to find the date of the event from possible date candidates obtained through
        Basis Time enrichment. Returns event_date in ISO format and also a possible quality score."""
        art_date = self.isodate(published_date)
        sntOffs = [k[0] for k in enumerate(enrichedFeed['BasisEnrichment']['tokens']) if k[1]['POS'] == 'SENT']
        offsRng = []
        for ph in enrichedFeed['keyPhrases']['phrasesPresent']['offsets']:
            found = False
            i = 0
            while i < (len(sntOffs) - 1):
                if sntOffs[i] > ph:
                    if i == 0:
                        offsRng.append((0, sntOffs[i]))
                    else:
                        offsRng.append((sntOffs[i - 1], sntOffs[i]))
                    found = True
                    break
                i += 1
            if not found and sntOffs:
                offsRng.append((sntOffs[-1], len(enrichedFeed['BasisEnrichment']['tokens']) - 1))
        dcand = []
        for dt in enrichedFeed['eventSemantics']['datetimes']:
            dt['offset'] = [int(k['offset'].split(':')[0]) for k in
                            enrichedFeed['BasisEnrichment']['nounPhrases']
                            if re.sub('[^\w]', '', dt['phrase']).lower() ==
                            re.sub('[^\w]', '', k['expr']).lower()]
            if dt['offset'] == []:
                dt['offset'] = [int(k['offset'].split(':')[0]) for k in
                                enrichedFeed['BasisEnrichment']['entities']
                                if re.sub('[^\w]', '', dt['phrase']).lower() in
                                re.sub('[^\w]', '', k['expr']).lower()]
            if dt['offset'] == []:
                log.error("offset for datetime Phrase \"%s\" could not be determined" % dt['phrase'])
#                print "offset not determined %s " % dt['phrase']
            else:
                dt['offset'] = dt['offset'][0]
                i = 0
                while i < (len(offsRng)):
                    if dt['offset'] < offsRng[i][1] and dt['offset'] > offsRng[i][0]:
                        if abs(dt['offset'] - enrichedFeed['keyPhrases']['phrasesPresent']['offsets'][i]) < 50:
                            dt['date'] = self.isodate(dt['date'])
                            if dt['date']:
                                dcand.append(dt)
                    i += 1
        if dcand == []:
            return ''
        fDates = [k for k in dcand if k['date'] > art_date]
        if not fDates:
            return ''
        minDate = min([k['date'] for k in fDates])
        return [k for k in fDates if k['date'] == minDate][0]

    def isodate(self, date):
        dt = None
        try:
            dtStr = re.findall(dateRE, date.strip())
            if dtStr:
                return datetime.strptime(dtStr[0], '%Y-%m-%d')
            else:
                return datetime.strptime(date.strip(), '%Y-%m')
        except ValueError:
            try:
                return datetime.strptime(date.strip(), '%Y')
            except ValueError:
                log.error('date could not be decoded: %s' % date)
#                print "date could not be decoded"
        if dt:
            return dt
        return None


def classify(content):
    '''Classifies input content into the corresponding protestType, type of
        population, nature of protest
    '''
    subtype = pTypeClassifier.classify(content)
    nature = pNatureClassifier.classify(content)
    popln = poplnClassifier.classify(content)
    return "01" + subtype + nature, POPULATION_TYPE_CODES[popln]


def jMarshal(readerObj):
    for line in readerObj:
        try:
            enrichedFeed = json.loads(line)
        except UnicodeEncodeError, e:
            log.debug("unable to read json %s \nfeed: %s" % (str(e), line))
            continue
        yield enrichedFeed


def main(readerObj, publisher, playback, surrogate, sourceType):
    """function main() iterating through feeds"""
    if SYS_IN:
        reader = jMarshal(readerObj)
    else:
        reader = readerObj
    for enrichedFeed in reader:
        w = warning('Planned Protest')
        artPubDate = str(datetime.utcnow().isoformat('T'))
        if playback is True:
            if 'publishedDate' in enrichedFeed:
                artPubDate = enrichedFeed['publishedDate']
            elif 'published_date' in enrichedFeed:
                artPubDate = enrichedFeed['published_date']
            elif 'date' in enrichedFeed:
                artPubDate = enrichedFeed['date']
        e = dateHandler()
        evtDate = e.getDate(enrichedFeed, artPubDate)
        if evtDate == '':
            log.debug('No date found for json for embersId: %s' % enrichedFeed['embersId'])
            continue
        enrichedFeed['evtDate'] = evtDate['date'].isoformat('T')
        if (e.isodate(enrichedFeed['evtDate']) - e.isodate(artPubDate)).days > 20:
            log.error('Date of event is too far in the future %s  for embersId %s ' % (str(enrichedFeed['evtDate']), enrichedFeed['embersId']))
            continue
        w.setEventDate(enrichedFeed['evtDate'])
        enrichedFeed['date'] = e.isodate(enrichedFeed['date']).isoformat('T')
        w.setDerivedFrom(enrichedFeed, sourceType)
        if strictGeo:
            w.setLocation(enrichedFeed['inferredLocation'])
        else:
            w.setLocation((enrichedFeed['country'], '', ''))
        w.setConfidence(1.0)
        if classifier_flag:
            evtType, popln = classify(' '.join([k['value'] for k in enrichedFeed['BasisEnrichment']['tokens']]))
        else:
            evtType, popln = '', ''
        w.setPopulation(popln)
        w.setEventType(evtType)
        if 'finalUrl' in enrichedFeed:
            url = enrichedFeed['finalUrl']
        elif 'url' in enrichedFeed:
            url = enrichedFeed['url']
        else:
            url = ''
        try:
            enrichedFeed['date'] = e.isodate(enrichedFeed['date']).isoformat('T')
            w.sendSurrogate(surrogate, evtDate['phrase'], enrichedFeed, sourceType,
                            enrichedFeed['keyPhrases']['resource'], url, SYS_OUT)
            if playback:
                w.send(publisher, enrichedFeed['date'], SYS_OUT, url)
            else:
                w.send(publisher, datetime.now().isoformat('T'), SYS_OUT, url)
        except Exception, e:
            log.error("Unable to publish to Queue: %s" % str(e))
            exit(1)
    return None

if __name__ == "__main__":
    ap = args.get_parser()
    ap.add_argument('--cat', default=False, action='store_true', help='Path to the Feeds file')
    ap.add_argument('-s', '--surrogate', default='tcp://*:8080', type=str,
                    help='the ZMQ port for the surrogate messages')
    ap.add_argument('-c', '--config', type=str, help='Path to the config directory')
    ap.add_argument('-t', '--sourceType', default='RSS', type=str, help='the source Feed Type-RSS/mailing-lists')
    ap.add_argument('-n', '--noclassifier', default=False, action='store_true', help='Flag to switch off classifier')
    ap.add_argument('-g', '--noGeo', default=False, action='store_true', help='Flag to switch off prediction of cities and locations')
    ap.add_argument('-p', '--playback', default=False, action='store_true',
                    help='Flag to switch on the playback mode')
    args = ap.parse_args()
    logs.init(args)
    queue.init(args)
    if args.noclassifier:
        classifier_flag = False
    if args.noGeo:
        strictGeo = False
    if args.cat:
        reader = codecs.getreader('utf-8')(sys.stdin)
        SYS_IN = True
    else:
        reader = queue.open(args.sub, 'r')
    if args.pub:
        writer = queue.open(args.pub, 'w', capture=True)
    else:
        SYS_OUT = True
        writer = codecs.getwriter('utf-8')(sys.stdout)
    if args.surrogate:
        surrogateWriter = queue.open(args.surrogate, 'w', capture=False)
    else:
        surrogateWriter = codecs.getwriter('utf-8')(sys.stdout)
    main(reader, writer, args.playback, surrogateWriter, args.sourceType)
    reader.close()
    writer.close()
    surrogateWriter.close()
