#!/usr/bin/env python
#-*- coding:utf-8 -*-
# vim: ts=4 sts=4 sw=4 tw=79 sta et
"""
    *.py: Description of what * does.
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

from etool import args
from dbHandler import dbConnect, columnFamily
from utils import Similarity, initializeIDF


def getdbConn(args):
    con = dbConnect(args.dbname, args.dbaddr)
    return con.getColumnFamily('Articles', 20)


def main(args):
    initializeIDF('config-newsthreader')
    db = getdbConn(args)
    doc1 = db.get(args.id1)
    doc2 = db.get(args.id2)
    print Similarity.cosineSimilarity(doc1[1]['freqVector'], doc2[1]['freqVector'])
    print doc1[1]['url']
    print doc2[1]['url']
    print doc1[1]['derivedFrom']
    print doc2[1]['derivedFrom']
    return None


if __name__ == "__main__":
    ap = args.get_parser()
    ap.add_argument('--id1', type=str)
    ap.add_argument('--id2', type=str)
    ap.add_argument('--dbaddr', default='128.173.223.230:9160', type=str)
    ap.add_argument('--dbname', default='Embers', type=str)
    args = ap.parse_args()
    main(args)
