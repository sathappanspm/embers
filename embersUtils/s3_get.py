#!/usr/bin/env python

import subprocess
import argparse
import boto
import os.path
import os
import codecs
import sys
import re

dateRE = re.compile('\d\d\d\d-\d\d-\d\d')


def dump_key(key, filename, fldr):
    # TODO fancify this a bit with a status callback the prints progress to stderr
    try:
        if filename == '-':
            f = codecs.getwriter('utf-8')(sys.stdout)
            key.get_file(f)
        else:
            with open("%s/%s" % (fldr, filename), 'w') as f:
                key.get_file(f)

    except Exception, e:
        sys.stderr.write('Caught: %s\n' % (str(e)))


def wccount(filename):
    out = subprocess.Popen(['wc', '-l', filename],
                           stdout=subprocess.PIPE,
                           stderr=subprocess.STDOUT
                           ).communicate()[0]
    return int(out.partition(b' ')[0])


def get_num_lines(k, fn):
    nLines = 0
    dump_key(k, fn)
    nLines = wccount(fn)
    os.remove(fn)
    return nLines


def main():
    ap = argparse.ArgumentParser("Get some files from S3.")
    ap.add_argument('-b', '--bucket', metavar='BUCKET', type=str, default='embers-osi-data',
                    help='S3 bucket to use')
    ap.add_argument('-p', '--prefix', metavar='PREFIX', type=str, nargs='+', default='',
                    help='Limit the summary to a specific prefix of keys.')
    ap.add_argument('-k', '--key', metavar='KEY', type=str, default=os.environ['AWS_ACCESS_KEY_ID'],
                    help='''The AWS key.''')
    ap.add_argument('-s', '--secret', metavar='SECRET', type=str, default=os.environ['AWS_SECRET_ACCESS_KEY'],
                    help='''The AWS key secret.''')
    ap.add_argument('-c', '--cat', action='store_true',
                    help='''Concatenate output to stdout instead of creating files.''')
    ap.add_argument('-l', '--list', action='store_true',
                    help='''Only list files instead of downloading them.''')
    ap.add_argument('-f', '--longlist', action='store_true',
                    help='''Only list files instead of downloading them, using full path.''')
    ap.add_argument('--start', type=str, default='', help="filter for files greater than this date")
    ap.add_argument('--end', type=str, default='', help="filer till this date")
    ap.add_argument('--folder', type=str, default='', help="folder")
    args = ap.parse_args()

    c_s3 = boto.connect_s3(args.key, args.secret)
    b = c_s3.get_bucket(args.bucket)
    for p in args.prefix:
        l = b.list(prefix=p)
        for k in l:
            if args.cat:
                fn = '-'
            else:
                fn = os.path.basename(k.name)
            print fn
            if args.start:
                dtStr = dateRE.findall(fn)[0]
                if dtStr < args.start:
                    continue
                if args.end:
                    if dtStr > args.end:
                        continue
            if args.list:
                print fn
            elif args.longlist:
                print k.name

            elif (fn == ''):
                print "Empty fn extracted from %s, skipping" % k.name
                continue
            else:
                dump_key(k, fn, args.folder)

if __name__ == "__main__":
    main()
