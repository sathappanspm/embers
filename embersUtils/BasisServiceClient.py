# -*- coding: utf-8 -*-
import zmq
import argparse
from feed_content_ingest import getContent
import codecs
import sys
import json
import langid
import numpy

class BasisEnricher(object):
    def __init__(self, socketId='tcp://54.234.11.20:5563'):
        self.context = zmq.Context(1)
        self.socket = self.context.socket(zmq.REQ)
        self.socket.connect(socketId)

    def requestJson(self, j):
        self.socket.send_json(j, 0)
        j = self.socket.recv_json(0)
        return j

    def requestStr(self, jsonStr):
        if not isinstance(jsonStr, unicode):
            jsonStr = jsonStr.decode('utf-8')
        self.socket.send_unicode(jsonStr)
        return self.socket.recv_unicode(jsonStr)

if __name__ == "__main__":
    parser =argparse.ArgumentParser()
    parser.add_argument('--cat', default='False', action='store_true', help ='Pass files using cat')
    parser.add_argument('--out', type=str, help = 'Output File name')
    parser.add_argument('--lib', type=str, help = 'Path to Jre')
    args = parser.parse_args()
    if args.cat:
        reader = codecs.getreader('utf-8')(sys.stdin)
    bS = BasisEnricher('tcp://54.234.11.20:5563')
    import os
    #os.environ['JAVA_HOME'] = arg.lib
    from boilerpipe.extract import Extractor as Extractor
    langmap = {'en': 'English', 'es': 'Spanish', 'pt': 'Portuguese'}
    with codecs.open(args.out, 'w', encoding='utf-8') as out:
        for line in reader:
            j = json.loads(line.strip())
            content = getContent(j['html_content'], Extractor)
            (lang, confidence) = langid.classify(content)
            bE = bS.requestJson({'text': content, 'lang': lang})
            j['lang'] = bE['lang']
            j['BasisEnrichment'] = bE['BasisEnrichment']
            out.write(json.dumps(j, encoding='utf-8'))
            out.write('\n')
