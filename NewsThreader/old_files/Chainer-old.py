# -*- coding:utf-8 -*-
from DbManager import DbManager
import json
from Similarity import Similarity 
from minQueue import minQueue
import uuid


class Chainer:
	def __init__(self,time_delta , n , threshold):
		self.db = DbManager()
		self.time_delta = time_delta
		self.consideration_size = n
		self.threshold = threshold
	
	def findMostSimilar(self,j):
		article_lang = j['description:lang']
		article_country = j['description:country']
		article_published = j['description:published_date']
		stop_id = article_lang + '_' + article_country + '_' + str(article_published+1) # add 1 to include articles of that day
		article_id = stop_id + '_' +j['description:embers_id']
		start_date = article_published - 3600*self.time_delta
		start_id = article_lang + '_' + article_country + '_' + str(start_date)
		scanner = self.db.scanner(start_id,stop_id,'RssNewsFeeds')
		#scanner = self.db.scanner()
		print "{} {}".format(start_id,stop_id)
		minq = minQueue(self.consideration_size)
		try:
			while True:
				next_article =scanner.next()
				next_article = self.convert_to_json(next_article)
				print " hio" + next_article[1]['description:embers_id']
				if next_article[1]['features:tokens'] is None:
					print "error"
				distance = Similarity.cosine_similarity(j['features:tokens'],next_article[1]['features:tokens'])
				if distance > 0.7:
					#the article is a duplicate
					if j['description:published_date'] == int(next_article[1]['description:published_date']) :
						print "duplicate"
					else:
						print "percolation"	
						#store this article in all chains of the original article
					#return False
				
				print "{} {}".format(distance,next_article[1]['description:embers_id'])
				if distance < self.threshold :
					continue
				combined_distance = self.get_combinedDistance(j,next_article)
				minq.append(next_article,combined_distance)
				
		except StopIteration:
			self.findTopStory(j,minq)
			print "hello"
		
		self.add_article(j)
		return True
					
	def findTopStory(self,j,minq):
		for k in minq:
			print k[0]
			print "helloee"
		if minq.isEmpty() :
			self.createNewStory(j,None)
		
		storyQ = minQueue(3)
		for db_article in minq:
			stories_list = db_article[1]['story_ids:ids']
			row_scanner = self.db.get_rows(stories_list,'stories')
			try:
				while True:
					story = row_scanner.next()
					story = self.convert_to_json(story)
					persons_distance = Similarity.jaccard_similarity(j['features:persons'],story[1]['features:persons'])
					org_distance = Similarity.jaccard_similarity(j['features:organisations'],story[1]['features:organisations'])
					entity_distance = (persons_distance + org_distance)/2
					location_distance = Similarity.jaccard_similarity(j['features:locations'],story[1]['features:locations'])
					total_distance = 0.4*entity_distance + 0.6*location_distance

					#to be added check distance with last entity of chain
				
					if total_distance > 0.4:
						storyQ.append(story,total_distance)
			except StopIteration :
				if storyQ.isEmpty():
					self.createNewStory(j,minq)
				else:
					self.addToStory(j,storyQ)

	def createNewStory(self,j,q):
		if q is None:
			new_story = {}
			new_story['members:list'] = [j['description:embers_id']]
			new_story['description:latest_date'] = j['description:published_date']
			new_story['features:persons'] = j['features:persons']
			new_story['features:organisations'] = j['features:organisations']
			new_story['features:locations'] = j['features:locations']
			self.write_chain(j,new_story,None) # None is to indicate that this is a New_story
			return
		else:
			#code to create a branched story.
			#creating a story out of the most similar articles
			new_story = {}
			new_story['members:list']= []
			new_story['description:latest_date'] = j['description:published_date']
			new_story['features:persons'] = {}
			new_story['features:organisations'] = {}
			new_story['features:locations'] = {}
			for next_article in q:
				new_story['members:list'].append(next_article[1]['description:embers_id'])
				time_diff = j['description:published_date'] - next_article[1]['description:published_date']  
				Similarity.update_dict(new_story['features:persons'] , next_article[1]['features:persons'],time_diff,True,7.0)	
				Similarity.update_dict(new_story['features:organisations'] , next_article[1]['features:organisations'],time_diff,True,7.0)	
				Similarity.update_dict(new_story['features:locations'] , next_article[1]['features:locations'],time_diff,True,7.0)	

			self.write_chain(j,new_story,None)	
			return


	def addToStory(self,j,storyQ):
		for story in storyQ:
			print story
			story = self.convert_to_json(story)
			story['members:list'].append(j['description:embers_id'])
			prev_latest = story['description:latest_date']
			story[0]['description:latest_date'] = j['description:published_date']
			tdecay = j['description:published_date'] - prev_latest
			Similarity.update_dict(story[0]['features:persons'],j['features:persons'],tdecay,True,7.0)
			Similarity.update_dict(story[0]['features:organisations'],j['features:organisations'],tdecay,7.0)
			Similarity.update_dict(story[0]['features:locations'],j['locations'],tdecay,False)
			self.write_chain(j,story,story[0])
		return

	def write_chain(self,article,story,story_id):
		if story_id is None:
			story_id = str(uuid.uuid5(uuid.NAMESPACE_X500,json.dumps(story)))
			#append(story id in all members)
		article['story_ids:ids'].append(story_id)
		#self.db.write_row(j,'RssNewsFeeds',j['description:embers_id'])
		self.db.write_row(story,'stories',story_id)
		return

	def add_article(self,j):
		article_id = j['description:lang'] + '_' + j['description:country'] + '_' +str(j['description:published_date']) +'_'+ j['description:embers_id']
		self.db.write_row(j,'RssNewsFeeds',article_id)
		return

	def close_db(self):
		self.db.close()	
	

	def get_combinedDistance(self,j,prev_article)	:
		distance = Similarity.cosine_similarity(j['features:tokens'],prev_article[1]['features:tokens'])
		
		#soergels distance calculated for entities in java version
		
		p_distance = Similarity.jaccard_similarity(j['features:persons'],prev_article[1]['features:persons'])
		org_distance = Similarity.jaccard_similarity(j['features:organisations'],prev_article[1]['features:organisations'])
		entity_distance = (p_distance + org_distance)/2.0
		loc_distance = Similarity.jaccard_similarity(j['features:locations'],prev_article[1]['features:locations'])
			
		combined_distance = 0.7*distance + 0.2*p_distance + 0.1*org_distance
		return combined_distance


				

	def convert_to_json(self,next_json):
		n=(next_json[0],{})
		for k in next_json[1]:
			n[1][k] = json.loads(next_json[1][k])
		return n	



				

