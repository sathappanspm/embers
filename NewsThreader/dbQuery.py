#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
    Last Modified:
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

from dbHandler import columnFamily, dbConnect
import argparse


class dbQuery:
    def __init__(self, dbname='EmbersNewFull', dbaddr='128.173.223.230:9160'):
        self.dbCon = dbConnect(dbname, host=dbaddr)
        self.chainDb = columnFamily(self.dbCon, 'Chains', 1, ['lastUpdateTime'])
        self.articleDb = columnFamily(self.dbCon, 'Articles', 1, ['publishedDate'])
        return

    def getRange(self, start, end, prefix):
        articles = self.articleDb.getRange('%s_%s' % (prefix, start), '%s_%s' % (prefix, end))
        for k, v in articles:
            self.getChains(v['chainIds'], start, end)
        return

    def get(self, chainKeyList=[]):
        cProp = self.chainDb.getMany(chainKeyList, ['letters'])
        for k in cProp:
            yield k

    def close(self):
        self.dbCon.close()
        return

    def getChains(self, chainKeyList, start, end):
        chains = self.chainDb.getMany(chainKeyList)
        for key, cProp in chains:
            ltrs = {}
            j = {key: ltrs}
            for dt in cProp['letters']:
                if dt >= start and dt <= end:
                    ltrs[dt] = cProp['letters'][dt]
            print j
        return

    def getUrl(self, chainId):
        chain = self.chainDb.get(chainId)
        articles = self.articleDb.getMany(chain[1]['elements'])
        for k in articles:
            print "%s   %s " % (k[1]['url'], k[1]['publishedDate'].isoformat())


def main(args):
    db = dbQuery(args.dbname, args.dbaddr)
    if args.id:
        db.getUrl(args.id)
    else:
        db.getRange(args.start, args.end, args.prefix)
    db.close()
    return


if __name__ == "__main__":
    ap = argparse.ArgumentParser()
    ap.add_argument('--start', type=str)
    ap.add_argument('--prefix', type=str)
    ap.add_argument('--end', type=str)
    ap.add_argument('--id', type=str)
    ap.add_argument('--dbname', default='', type=str)
    ap.add_argument('--dbaddr', default='', type=str)
    args = ap.parse_args()
    main(args)
