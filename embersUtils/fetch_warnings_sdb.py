#!/usr/bin/env python
# -*- coding: UTF-8 -*-
# vim: ts=4 sts=4 sw=4 tw=79 sta et
import os
import argparse
import boto
import json
from datetime import datetime
# this assumes AWS_ACCESS_KEY_ID and AWS_SECRET_ACCESS_KEY are in your
# environment


def format_warning(warning):
    warning['confidence'] = float(warning['confidence'])
    warning['confidenceIsProbability'] = bool(warning['confidenceIsProbability'])
    if isinstance(warning['location'], basestring):
        warning['location'] = eval(warning['location'])
    warning['location'] = [l.decode('unicode_escape') for l in warning['location']]
    if isinstance(warning['derivedFrom'], basestring):
        warning['derivedFrom'] = eval(warning['derivedFrom'])
    if 'coordinates' in warning and \
       isinstance(warning['coordinates'],basestring):
        warning['coordinates'] = eval(warning['coordinates'])
    return warning


def get_cu_warnings(key=os.environ['AWS_ACCESS_KEY_ID'],
                    secret=os.environ['AWS_SECRET_ACCESS_KEY'],
                    model='',
                    fromdate='2012-01-01',
                    todate=datetime.now().strftime("%Y-%m-%d"),
                    incl=True, print2file=None):
    conn = boto.connect_sdb(key, secret)
    dom = conn.get_domain('warnings')
    gt,lt = ">", "<"
    if incl:
        gt,lt = ">=","<="

    fro = "eventDate %s '%s' " % (gt, fromdate)
    to  = "AND eventDate %s '%s' " % (lt, todate)
    fro_to = "%s %s" % (fro,to)
    stmnt = "SELECT * FROM warnings WHERE %s " % fro_to + "AND eventType like '01%'"
    result = dom.select(stmnt)
    warnings = [format_warning(w) for w in result]
    print len(warnings)
    if print2file:
        f = open("warnings-%s_%s" % (fromdate,todate),'w')
        print "file opened to write"
        for w in warnings:
            if not model:
                f.write(json.dumps(w) + '\n')
            elif model in w['model'].lower():
                f.write(json.dumps(w)+'\n')
        f.close()
    else:
        return warnings


def main():
    ap = argparse.ArgumentParser("Get Civil Unrest warnings from S3's SimpleDb.")
    ap.add_argument('-f', '--fromdate', metavar='FROMDATE', type=str, default='2012-01-01',
                    help='date from which warnings to select')
    ap.add_argument('-t', '--todate', metavar='TODATE', type=str,
                    default=datetime.now().strftime("%Y-%m-%d"),
                    help='date to which warnings to select')
    ap.add_argument('-k', '--key', metavar='KEY', type=str,
                    default=os.environ['AWS_ACCESS_KEY_ID'],
                    help='''The AWS key.''')
    ap.add_argument('-s', '--secret', metavar='SECRET', type=str,
                    default=os.environ['AWS_SECRET_ACCESS_KEY'],
                    help='''The AWS key secret.''')
    ap.add_argument('-i', '--incl', action='store_false', default=True,
                    help='''make from and to date non-inclusive''')
    ap.add_argument('-p', '--print2file', action='store_true', default=False,
                    help='''print warnings to json file''')
    ap.add_argument('-m', '--model', type=str, help="model name")
    args = ap.parse_args()
    get_cu_warnings(args.key, args.secret, args.model, args.fromdate, args.todate,
                    args.incl, args.print2file)


if __name__ == "__main__":
    main()
