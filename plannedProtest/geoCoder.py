#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""
import re
from etool import logs, queue, args
import Normalize
from Normalize import normalize_entity
import json
from datetime import datetime
import sys

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "1.0.2v1"
__processor__ = "geoCoder.py"

log = logs.getLogger('%s-%s.log' % (__processor__, str(datetime.now())))
SYS_IN = False
SYS_OUT = False
COUNTRY_CODES = {'ar': "argentina", 'bz': 'belize', 'bo': 'bolivia',
                 'cl': 'chile', 'co': 'colombia', 'cr': 'costa rica',
                 'ec': 'ecuador', 'sv': 'el salvador', 'gf': 'french guiana',
                 'gt': 'guatemala', 'gy': 'guyana', 'hn': 'honduras',
                 'ni': 'nicaragua', 'pa': 'panama', 'py': 'paraguay',
                 'pe': 'peru', 'sr': 'suriname', 'uy': 'uruguay',
                 've': 'venezuela', 'br': 'brazil', 'mx': "mexico",
                 }

countryRE = re.compile('\\b(%s)\\b' % ('|'.join(COUNTRY_CODES.keys())))

def evaluate(f):
    from unidecode import unidecode
    cntry = 0
    state = 0
    city = 0
    na = 0
    tcnt = 0
    bsError = 0
    with open(f) as fl:
        for l in fl:
            tcnt += 1
            j = json.loads(l, encoding='utf-8')
            if unidecode(j['trueLocation'][0].lower()) == j['inferredLocation'][0].lower():
                cntry += 1
                if unidecode(j['trueLocation'][1].lower()) == j['inferredLocation'][1].lower():
                    state += 1
                if unidecode(j['trueLocation'][2].lower()) == j['inferredLocation'][2].lower():
                    city += 1
            if j['inferredLocation'][0] == '' or j['inferredLocation'][0] == 'NA':
                na += 1
            if not j['BasisEnrichment']['tokens']:
                bsError += 1
    print "country level precision %s" % cntry
    print "state level precision %s" % state
    print "city level precision %s" % city
    print " no geocode %s" % na
    print " no enrichment %s" % bsError
    print "Total number of articles %s" % tcnt
    return


class eventSemantics(object):
    @staticmethod
    def initDict(city_map_file, entity_stopwords_file):
        """Initiates the lookup dictionaries for Locations and also the stop word list for entities"""
        with open(city_map_file) as f:
            eventSemantics.__city_map = json.load(f)
        with open(entity_stopwords_file) as f:
            eventSemantics.__entityStopList = json.load(f)
        return

    def __init__(self):
        pass

    @staticmethod
    def getReportedLocation(entity_dict):
        minOffsetLoc = 'NA'
        minOffset = 100
        if entity_dict['non-latinAmerica']:
            NAfiltered = entity_dict['non-latinAmerica']
            minOffset = min([NAfiltered[k]['offset'] for k in NAfiltered])
            minOffsetLoc = [k for k in NAfiltered if NAfiltered[k]['offset'] == minOffset][0]
        if entity_dict["locations"]:
            cityOffsets = {entity_dict["locations"][k]['offset']: k for k in entity_dict["locations"]
                           if entity_dict["locations"][k]['offset']}
            if cityOffsets:
                minCityOffset = min(cityOffsets)
                if minCityOffset > minOffset:
                    return ['NA', 'NA', minOffsetLoc]
                reportedCity = cityOffsets[min(cityOffsets)]
                countryOffsets = {entity_dict["country"][k]['offset']: k for k in entity_dict["country"]
                                  if entity_dict["country"][k]['explicit']
                                  and k in eventSemantics.__city_map[reportedCity]}
                if countryOffsets:
                    reportedCountry = countryOffsets[min(countryOffsets)]
                    reportedState = eventSemantics.__city_map[reportedCity][reportedCountry].keys()[0]
                    return [reportedCountry, reportedState, reportedCity]
        return ['NA', 'NA', minOffsetLoc]

    @staticmethod
    def inferLocation(entity_dict, noOfTokens):
        """A function to geo-code a given news or other sources content"""
        countryCnts = {k: entity_dict['country'][k]['count'] for k in
                       entity_dict['country']}   #if entity_dict['country'][k]['explicit'] }
        #                       and (False if entity_dict['country'][k]['offset'] < 20 and
        #                    entity_dict['country'][k]['count'] == 1.0 else True)}
        if entity_dict['non-latinAmerica']:
            NAfiltered = entity_dict['non-latinAmerica']
            if NAfiltered:
                NAvalues = [NAfiltered[k]['count'] for k in NAfiltered]
                maxVal = max(NAvalues)
                maxLoc = [k for k in NAfiltered if NAfiltered[k]['count'] == maxVal][0]
                if countryCnts:
                    if maxVal > (max(countryCnts.values()) + 1.0) and max(countryCnts.values()) < 3.0:
                        return ['', '', maxLoc], ''
                    else:
                        if max(countryCnts.values()) == 1.0:
                            maxOffset = max([entity_dict['country'][k]['offset'] for k in countryCnts])
                            if maxOffset < 20 and any([True for k in NAfiltered
                                                       if NAfiltered[k]['offset'] > 20]):
                                return ['', '', maxLoc], ''
                        numLA = sum([entity_dict['locations'][k]['count'] for k in entity_dict['locations']])
                        numLA += sum([entity_dict['states'][k]['count'] for k in entity_dict['states']])
                        numLA += sum([entity_dict['country'][k]['count'] for k in entity_dict['country']])
                        if sum(NAvalues) > numLA + 3:
                            return ['', '', maxLoc], ''
                else:
                    return ['', '', maxLoc], ''
        if countryCnts:
            maxCountry = {k: entity_dict['country'][k]['count'] for k, v in countryCnts.items()}
            countryCands = {k: entity_dict['country'][k]['offset'] for k in maxCountry if maxCountry[k] == max(maxCountry.values())}
            maxState = {}
            locMentioned = [k for k in entity_dict["locations"]]
            stateMentioned = [k for k in entity_dict["states"]]
            for cntry in countryCands:
                for s in stateMentioned:
                    if cntry in eventSemantics.__city_map['state'][s]:
                        maxState[(cntry, s)] = entity_dict['states'][s]['count']
                for l in locMentioned:
                    if cntry in eventSemantics.__city_map[l]:
                        for s in eventSemantics.__city_map[l][cntry]:
                            if (cntry, s) in maxState:
                                maxState[(cntry, s)] += entity_dict['locations'][l]['count']
                                if entity_dict['states'][s]['offset'] > entity_dict['locations'][l]['offset']:
                                    entity_dict['states'][s]['offset'] = entity_dict['locations'][l]['offset']
                            else:
                                maxState[(cntry, s)] = entity_dict['locations'][l]['count']
                                entity_dict['states'][s] = entity_dict['locations'][l]
            if not maxState:
                cntrySel = [k for k in countryCands if countryCands[k] == min(countryCands.values())][0]
                return [cntrySel, '', ''], cntrySel
            stateCand = {k: entity_dict['states'][k[1]]['offset'] for k in maxState
                         if maxState[k] == max(maxState.values())}
            maxLoc = {}
            for s in stateCand:
                for l in locMentioned:
                    if s[0] in eventSemantics.__city_map[l] and s[1] in eventSemantics.__city_map[l][s[0]]:
                        locTuple = (s[0], s[1], eventSemantics.__city_map[l][s[0]][s[1]], l)
                        if locTuple in maxLoc:
                            maxLoc[locTuple] += 1.0
                        else:
                            maxLoc[locTuple] = 1.0
            if not maxLoc:
                stateSel = [k for k in stateCand if stateCand[k] == min(stateCand.values())][0]
                return [stateSel[0], stateSel[1], ''], stateSel[0]
            locCand = {k: entity_dict['locations'][k[3]]['offset'] for k in maxLoc
                       if maxLoc[k] == max(maxLoc.values())}
            locSel = [k for k in locCand if locCand[k] == min(locCand.values())][0]
            return [locSel[0], locSel[1], locSel[2]], locSel[0]
        else:
            return ['NA', 'NA', 'NA'], ''


def getDomainCountry(article):
    urlkey = 'finalurl'
    try:
        if 'finalUrl' in article:
            urlkey = 'finalUrl'
        article['url'] = article[urlkey] if urlkey in article else (article['url'] if
                                                                    'url' in article else article['contentURL'])
        reObj = re.findall('^http://[\w+\-:\.]*/', article['finalurl'])
    except Exception:
        article["country"] = None
        return None
    if reObj:
        country = re.findall(countryRE, reObj[0])
        if country:
            article['country'] = COUNTRY_CODES[country[0]]
            return article['country']
    if 'country' in article:
        try:
            article['country'] = COUNTRY_CODES[article['country']]
            return article['country']
        except Exception, e:
            log.warning('country code \"%s\" not recognizable,  errorMsg: %s' % (article['country'], e))
    article['country'] = None
    return article['country']


def jMarshal(readerObj):
    lineNo = 0
    for line in reader:
        lineNo += 1
        try:
            enrichedFeed = json.loads(line.strip(), encoding='utf-8')
        except ValueError, e:
            log.debug("unable to read json %s on line number %s" % (str(e), str(lineNo)))
            continue
        yield enrichedFeed


def main(readerObj, writer):
    """function main() iterating through feeds"""
    if SYS_IN:
        reader = jMarshal(readerObj)
    else:
        reader = readerObj
    for enrichedFeed in reader:
        getDomainCountry(enrichedFeed)
        entity_dict = normalize_entity(enrichedFeed, enrichedFeed["country"])
        noOfTokens = len(enrichedFeed['BasisEnrichment']['tokens'])

        enrichedFeed['reportedFrom'] = eventSemantics.getReportedLocation(entity_dict)
        enrichedFeed['inferredLocation'], enrichedFeed['country'] = eventSemantics.inferLocation(entity_dict,
                                                                                                 noOfTokens)
        if 'embers_id' in enrichedFeed:
            enrichedFeed['embersId'] = enrichedFeed['embers_id']
        enrichedFeed['embersId'] = enrichedFeed['embersParentId'] if enrichedFeed['embersParentId'] else enrichedFeed['embersId']
        enrichedFeed['derivedFrom'] = {'derivedIds': [enrichedFeed['embersId']]}
        enrichedFeed['embersGeoCode'] = {"admin1": enrichedFeed['inferredLocation'][1].title(),
                                         "city": enrichedFeed['inferredLocation'][2].title(),
                                         "country": enrichedFeed['inferredLocation'][0].title()}
        enrichedFeed['BasisEnrichment']['geoCodeEnrichment'] = {'process': __processor__, 'version': __version__}
        enrichedFeed['BasisEnrichment']['geoCodeEnrichment']['states'] = entity_dict['states']
        enrichedFeed['BasisEnrichment']['geoCodeEnrichment']['locations'] = entity_dict['locations']
        enrichedFeed['BasisEnrichment']['geoCodeEnrichment']['persons'] = entity_dict['persons']
        enrichedFeed['BasisEnrichment']['geoCodeEnrichment']['organisations'] = entity_dict['organisations']
        enrichedFeed['BasisEnrichment']['geoCodeEnrichment']['non-latinAmerica'] = entity_dict['non-latinAmerica']
        if SYS_OUT:
            #print enrichedFeed['embersGeoCode']
            writer.write(json.dumps(enrichedFeed, encoding='utf-8'))
            writer.write('\n')
        else:
            writer.write(enrichedFeed)
    return None


if __name__ == "__main__":
    ap = args.get_parser()
    ap.add_argument('--cat', action='store_true', help='Flag to read input from stdin')
    ap.add_argument('-c', '--config', type=str, help='path to the config files directory')
    ap.add_argument('--eval', type=str, help="filename to be evaluated")
    log = logs.getLogger('%s-%s.log' % (__processor__, str(datetime.now())))
    args = ap.parse_args()
    logs.init(args)
    queue.init(args)
    global CONFIG_DIR
    if args.eval:
        evaluate(args.eval)
        exit(0)
    if args.config is not None:
        CONFIG_DIR = args.config
    else:
        import os
        CONFIG_DIR = os.path.join(os.path.dirname(__file__), 'config-planned-protest')
    if args.cat:
        reader = sys.stdin
        SYS_IN = True
    else:
        reader = queue.open(args.sub, 'r')
    if args.pub:
        writer = queue.open(args.pub, 'w', capture=False)
    else:
        writer = sys.stdout
        SYS_OUT = True
    try:
        eventSemantics.initDict('%s/city_data.txt' % CONFIG_DIR, '%s/es/entitiesStopword.txt' % CONFIG_DIR)
        Normalize.init('%s/city_data.txt' % CONFIG_DIR, '%s/es/entitiesStopword.txt' % CONFIG_DIR)
    except IOError, e:
        log.error('Unable to read Config Directory %s. \nExitting' % str(e))
        exit(1)
    try:
        main(reader, writer)
    except KeyboardInterrupt:
        sys.stderr.write("Exitting...keyboard Interrupt")
    reader.close()
    writer.close()
