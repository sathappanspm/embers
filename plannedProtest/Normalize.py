#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
    Last Modified:Mon Jan 28 13:07:24 2013
"""
from etool import logs
from datetime import datetime
import json
import re
import codecs
from unidecode import unidecode

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "1.0.0v1"
__processor__ = "Normalizer"

log = logs.getLogger("%s-%s.log" % (__processor__, str(datetime.now())))
logs.init()

CITY_MAP = {}
ENTITY_STOPLIST = {}
stringRE = re.compile('[^a-zA-Z\s_]+')
PEOPLE_LIST = {'panamena': 'panama', 'mexican': 'mexico', 'peruvian': 'peru', 'nicaraguans': 'nicaragua', 'surinam': 'suriname', 'ecuadorian': 'ecuador',
               'beliceno': 'belize', 'guatemalteco': 'guatemala', 'uruguayan': 'uruguay', 'nicaraguense': 'nicaragua', 'mexicana': 'mexico',
               'chilena': 'chile', 'paraguayos': 'paraguay', 'panameno': 'panama', 'guyanese': 'guyana', 'guatemalteca': 'guatemala',
               'guyanesa': 'guyana', 'mexicano': 'mexico', 'paraguayans': 'paraguay', 'argentino': 'argentina', 'guatemalan': 'guatemala',
               'brasilena': 'brazil', 'brazilian': 'brazil', 'brasileno': 'brazil', 'argentine': 'argentina', 'surinames': 'suriname',
               'salvadorena': 'el salvador', 'ecuatoriano': 'ecuador', 'peruana': 'peru', 'peruano': 'peru', 'ecuatoriana': 'ecuador', 'belicena': 'belize',
               'venezolana': 'venezuela', 'bolivian': 'bolivia', 'argentinian': 'argentina', 'paraguaya': 'paraguay', 'costarricense': 'costa rica',
               'chilean': 'chile', 'brasil': 'brazil', 'salvador': 'el salvador', 'uruguayo': 'uruguay', 'boliviano': 'bolivia', 'uruguaya': 'uruguay',
               'guyanes': 'guyana', 'belice': 'belize', 'hondureno': 'honduras', 'colombiana': 'colombia', 'colombiano': 'colombia', 'hondurena': 'honduras',
               'mexicanos': 'mexico', 'mexicano': 'mexico', 'nicaraguan': 'nicaragua', 'surinamese': 'suriname', 'venezuelan': 'venezuela',
               'peruvian': 'peru', 'colombian': 'colombia', 'chileno': 'chile', 'venezolano': 'venezuela'}

commonPlacesRE = re.compile('\\bplaza\\b', re.I)
PEOPLE_RE = re.compile('\\b(%s)s{0,1}\\b' % ('|'.join(PEOPLE_LIST.keys())), re.I)


def init(cityMap, entityStopList):
    global CITY_MAP, ENTITY_STOPLIST
    with codecs.open(cityMap, 'r', encoding='utf-8') as f:
        CITY_MAP = json.load(f, encoding='utf-8')
    with codecs.open(entityStopList, 'r', encoding='utf-8') as f:
        ENTITY_STOPLIST = json.load(f, encoding='utf-8')
    return


def normalize_str(s):
    if isinstance(s, str):
        return unidecode(s.lower().decode('utf-8').strip())
    return unidecode(s.lower().strip())


def normalize_entity(enrichedFeed, domainCountry):
    entity_dict = {'locations': {}, 'country': {}, 'states': {},
                   'persons': {}, 'organisations': {}, 'non-latinAmerica': {}}
    blacklist = [u'madrid', u'barcelona', u'california', u'los angeles', u'california', u'san francisco']
    entityList = enrichedFeed['BasisEnrichment']['entities']
    offsetThreshold = 1.00
    noOfTokens = len(enrichedFeed['BasisEnrichment']['tokens'])
    for k in entityList:
        entity = stringRE.sub('', normalize_str(k['expr']))
        offset = int(k['offset'][:k['offset'].find(':')])
        if entity in ENTITY_STOPLIST or len(entity) < 3 or "editado" in entity:
            continue
        if offset > offsetThreshold * noOfTokens:
            continue
        if k['neType'] == 'LOCATION':
            insertLoc(entity, entity_dict, offset)
        elif k['neType'] == 'PERSON':
            insertNE(entity, entity_dict, True, offset)
        elif k['neType'] == 'ORGANIZATION':
            insertNE(entity, entity_dict, True, offset)
    if entity_dict['country'] or not entity_dict['non-latinAmerica']:
        noOfCountries = len(entity_dict['country'])
        explicitCountries = [k for k in entity_dict['country'] if entity_dict['country'][k]['explicit']]
        if not explicitCountries:
            if not any([True for k in blacklist if k in entity_dict['locations']]):
                if domainCountry in entity_dict["country"]:
                    if entity_dict["country"][domainCountry]['count'] != 1.0 and noOfCountries:
                        entity_dict['country'][domainCountry]['explicit'] = True
            else:
                for disputedLoc in blacklist:
                    if disputedLoc in entity_dict["locations"]:
                        del entity_dict["locations"][disputedLoc]
                    if disputedLoc in entity_dict["states"]:
                        del entity_dict["states"][disputedLoc]
                if len(entity_dict['non-latinAmerica']) < 2.0:
                    if domainCountry in entity_dict["country"]:
                        if entity_dict["country"][domainCountry]['count'] != 1.0 and noOfCountries:
                            entity_dict['country'][domainCountry]['explicit'] = True
        elif domainCountry:
            if domainCountry in entity_dict["country"]:
                entity_dict["country"][domainCountry]['explicit'] = True
            else:
                if not entity_dict['country']:
                    entity_dict["country"][domainCountry] = {'count': 1.0, 'offset': 0,
                                                             'explicit': True}
        tokenLength = len(enrichedFeed['BasisEnrichment']['tokens'])
        textContent = ' '.join([k['value'] for k in
                                enrichedFeed['BasisEnrichment']['tokens'][0: int(1.0 * tokenLength)]])
        peoplesMatch = re.findall(PEOPLE_RE, unidecode(textContent))
        for k in peoplesMatch:
            c = PEOPLE_LIST[k.lower()]
            if c in entity_dict['country']:
                entity_dict["country"][c]['explicit'] = True
                entity_dict["country"][c]['count'] += 1.0
            else:
                entity_dict["country"][c] = {'explicit': True, 'count': 1.0, 'offset': len(enrichedFeed['BasisEnrichment']['tokens'])}
    return entity_dict


def insertLoc(word, j, offset):
    """a helper function to identify true location of text. location includes the city,state and
        country information."""
    if word == 'state' or word == 'country':
        return
    if word in j["locations"]:
        j["locations"][word]['count'] += 1.0
        if j['locations'][word]['offset'] > offset:
            j['locations'][word]['offset'] = offset
        for k in CITY_MAP[word]:
            if k in j["country"]:
                j["country"][k]['count'] += 1.0
                if offset < j['country'][k]['offset']:
                    j['country'][k]['offset'] = offset
        return
    if word in CITY_MAP["country"]:
        word = CITY_MAP["country"][word]
        if word in j["country"]:
            j['country'][word]['explicit'] = True
            j["country"][word]['count'] += 1.0
            if offset < j['country'][word]['offset']:
                j['country'][word]['offset'] = offset
        else:
            j["country"][word] = {'count': 1.0, 'explicit': True, 'offset': offset}
        return

    if word in CITY_MAP:
        j["locations"][word] = {'count': 1.0, 'offset': offset}
        for k in CITY_MAP[word]:
            if k in j["country"]:
                j["country"][k]['count'] += 1.0
                if offset < j['country'][k]['offset']:
                    j['country'][k]['offset'] = offset
            else:
                j["country"][k] = {'count': 1.0, 'explicit': False, 'offset': offset}
        return
    if word in CITY_MAP["state"]:
        if word in j["states"]:
            j["states"][word]["count"] += 1.0
            if offset < j["states"][word]['offset']:
                j["states"][word]['offset'] = offset
        else:
            j["states"][word] = {"count": 1.0, "offset": 1.0}
        for k in CITY_MAP["state"][word]:
            if k in j["country"]:
                j["country"][k]['count'] += 1.0
                if offset < j['country'][k]['offset']:
                    j['country'][k]['offset'] = offset
            else:
                j["country"][k] = {'count': 1.0, 'offset': offset, 'explicit': False}
    if not commonPlacesRE.match(word):
        if word in j['non-latinAmerica']:
            j['non-latinAmerica'][word]['count'] += 1.0
            if offset < j['non-latinAmerica'][word]['offset']:
                j['non-latinAmerica'][word]['offset'] = offset
        else:
            if len(word.split()) < 3:
                j['non-latinAmerica'][word] = {'count': 1.0, 'offset': offset}
    return


def insertNE(w, j, flag, offset):
    """A function performing coreference resolution for entities in a given text."""
    if w in ENTITY_STOPLIST:
        return
    for k in w.split():
        if k in CITY_MAP['country']:
            insertLoc(k, j, offset)
        #if k in CITY_MAP['state']:
        #    insertLoc(k, j, offset)
        #if k in CITY_MAP:
        #    insertLoc(k, j, offset)
    if flag:
        if w in j["persons"]:
            j["persons"][w] += 1.0
            return
        for k in j["persons"]:
            if w in k:
                j["persons"][k] += 1.0
                return
            if k in w:
                j["persons"][w] = j["persons"][k] + 1.0
                del j["persons"][k]
                return
        if w in CITY_MAP['country']:
            insertLoc(w, j, offset)
        j["persons"][w] = 1.0
        return
    else:
        if w in j["organisations"]:
            j["organisations"][w] += 1.0
            return
        if w in j["persons"]:
            j["persons"][w] += 1.0
            return
        for k in w.split(" de "):
            if k in CITY_MAP:
                insertLoc(k, j, offset)
        words = w.split(" ")
        if  len(words) > 1:
            for k in w.split(" "):
                if k in CITY_MAP["country"]:
                    insertLoc(k, j, offset)
        j["organisations"][w] = 1.0
        return
