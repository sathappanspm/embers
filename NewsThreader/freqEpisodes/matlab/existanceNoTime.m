function result = existanceNoTime(temp,chain),
    i = 1;
    cnt = 0;
    for t=1:length(chain),
        if chain(t) == temp(i),
            cnt = cnt + 1;
            i = i + 1;
        end
        if i>length(temp),
            break;
        end
    end
    if cnt==length(temp),
        result = 1;
    else
        result = 0;
    end
    return
end