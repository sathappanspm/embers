#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = ""
from dbHandler import dbConnect, columnFamily


#def isodate(date):
#    dt = None
#    try:
#        dt = datetime.strptime(re.findall(dateRE, date)[0], '%Y-%m-%d')
#    except ValueError, e:
#        log.error('date could not be decoded: %s, %s' % (date, e))
#    return dt


def connect():
    dbCon = dbConnect('embers', '128.173.223.230')
    return columnFamily(dbCon, 'Chains')


def get(chainIds, cf):
    return cf.getMany(chainIds)


def main(args):
    cf = connect()
    episodes = open('/home/vicky/DataSets/frequentEpisodes/JSON2012-09-01-15.txt')
    locVector = {}
    for k in episodes:
        j = eval(k)
        epiKey = j.keys()
        epiKey.remove('chainids')
        epiKey.remove('episode')
        for key, ch in get(j['chainids'], cf):
            for l in ch['locations']:
                if ch['locations'][l] != 0:
                    if l in locVector:
                        locVector[l].append(epiKey[0])
                    else:
                        locVector[l] = [epiKey[0]]

    print locVector

if __name__ == "__main__":
    import sys
    main(sys.argv)
