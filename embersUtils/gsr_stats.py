import json

a = open('gsr_2013-02.data')
j = json.loads(a.next())
stats = {}
for k in a:
    j = json.loads(k)
    if j['location'][0] in stats:
        stats[j['location'][0]].append(j)
    else:
        stats[j['location'][0]] = [j]
mx = stats['Mexico']
mx_states = {}
mx_city = {}
for k in mx:
    ste = k['location'][1]
    cty = k['location'][2]
    dt = k['eventDate']
    if ste in mx_states:
        if dt in mx_states[ste]:
            mx_states[ste][dt] += 1
        else:
            mx_states[ste][dt] = 1
    else:
        mx_states[ste] = {dt: 1}
    if cty in mx_city:
        if dt in mx_city[cty]:
            mx_city[cty][dt] += 1
        else:
            mx_city[cty][dt] = 1
    else:
        mx_city[cty] = {dt: 1}
st_names = set(mx_states.keys())
dts = set()
[dts.update(mx_states[k].keys()) for k in mx_states]
out = open('gsr_cu_mx_distribution.tsv', 'w')
st_names = list(st_names)
dts = list(dts)
out.write('\t'.join(['date'] + st_names).encode('utf-8'))
out.write('\n')
for dt in dts:
    out.write(dt[0:10])
    out.write('\t')
    for k in st_names:
        if dt in mx_states[k]:
            out.write(str(mx_states[k][dt]))
        else:
            out.write("0")
        out.write('\t')
    out.write('\n')
