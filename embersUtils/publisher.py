#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = ""

from etool import args, queue
import json
import time

outFile = None

def main(args):
    if args.dump:
        global outFile
        q = queue.open(args.sub)
        cnt = 0
        binNo = 0
        start_name = "rss-geocoded-%s" % args.prefix
        outFile = open('%s%s-%s.txt' % (args.dir, start_name, binNo), 'w')
        fullcnt = 0
        for j in q:
            fullcnt += 1
            try:
                if args.filter:
                    if j["country"].lower() != args.filter:
                        continue
                outFile.write(json.dumps(j, encoding='utf-8'))
                outFile.write('\n')
                cnt += 1
            except Exception, e:
                print "error  %s e" % str(e)
            if cnt > 5000:
                print "full cnt %s binNum %s" % (fullcnt, binNo)
                cnt = 0
                outFile.close()
                binNo += 1
                outFile = open('%s%s-%s.txt' % (args.dir, start_name, binNo), 'w')
    else:
        q = queue.open(args.pub, 'w')
        cnt = 0
        if args.fullPath:
            args.dir = ''
        if args.limit:
            limit = True
        else:
            limit = False

        for f in open(args.list):
            print "%s   %s" % (f, cnt)
            try:
                with open('%s%s' % (args.dir, f.strip())) as inpFile:
                    for line in inpFile:
                        try:
                            j = json.loads(line.strip())
                        except Exception, e:
                            print "error %s" % str(e)
                            continue
                        if limit and args.limit > cnt:
                            exit(0)
                        cnt += 1
                        q.write(j)
                        #print cnt
                time.sleep(1)
            except Exception, e:
                print "Exception %s " % str(e)
                continue

if __name__ == "__main__":
    ap = args.get_parser()
    ap.add_argument('--dir', type=str, default='./')
    ap.add_argument('--list', type=str)
    ap.add_argument('--limit', type=int)
    ap.add_argument('--fullPath', action='store_true', default=False)
    ap.add_argument('--dump', action='store_true', default=False)
    ap.add_argument('--filter', type=str)
    ap.add_argument('--prefix', type=str)
    args = ap.parse_args()
    try:
        main(args)
    except KeyboardInterrupt:
        if outFile:
            outFile.close()
        print "closing Queue"
        exit(0)
