#!/usr/bin/env python
#-*- coding:utf-8 -*-
"""
    *.py: Description of what * does.
    Last Modified:
"""
__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

import json
from math import log as logarithm
from utils import Vector
import re
from etool import queue, args

docFreq = {'TOKENS': {}, 'docCount': 0}
stringRE = re.compile('[^a-zA-Z\s_]+')


def getDocCount(article):
    basisTag = 'BasisEnrichment' if 'BasisEnrichment' in article else 'basisenrichment'
    for word in article['freqVector']:
        if word not in docFreq['TOKENS']:
            docFreq['TOKENS'][word] = 0.0
        docFreq['TOKENS'][word] += 1.0
    for typ in ['persons', 'organisations', 'locations', 'states']:
        for k in article[basisTag]['geoCodeEnrichment'][typ]:
            if len(word.split(' ')) < 2:
                continue
            if word in docFreq['TOKENS']:
                docFreq['TOKENS'][word] += 1.0
            else:
                docFreq['TOKENS'][word] = 1.0
    docFreq['docCount'] += 1.0
    return


def calculate_idf(docFreq):
    totalDocs = docFreq['docCount']
    idf = {'TOKENS': {}, 'maxIdf': 0.0}
    for k in docFreq['TOKENS']:
        idf['TOKENS'][k] = logarithm(totalDocs / (docFreq['TOKENS'][k]))
        if idf['maxIdf'] < idf['TOKENS'][k]:
            idf['maxIdf'] = idf['TOKENS'][k]
    return idf


def main(args):
    try:
        if args.sub:
            reader = queue.open(args.sub)
            cnt = 0
            vector = Vector(args.config, False)
            for j in reader:
                cnt += 1
                basisTag = 'BasisEnrichment' if 'BasisEnrichment' in j else 'basisenrichment'
                if not j[basisTag]['language']:
                    j[basisTag]['language'] = 'spanish'
                j['freqVector'], alpha = vector.vectorize(j[basisTag]['tokens'], j[basisTag]['language'])
                print cnt
                getDocCount(j)
    except KeyboardInterrupt:
        print "writing idf file to disk"
    if args.docfreq:
        with open(args.docfreq) as df:
            idf = calculate_idf(json.loads(df.read(), encoding='utf-8'))
    else:
        with open(args.dfOut, 'w') as out:
            out.write(json.dumps(docFreq, encoding='utf-8'))
        idf = calculate_idf(docFreq)
    with open(args.out, 'w') as idfOut:
        idfOut.write(json.dumps(idf, encoding='utf-8'))
    return

if __name__ == "__main__":
    import os
    parser = args.get_parser()
    parser.add_argument('--dfOut', type=str, help="Name of output docFreq file")
    parser.add_argument('-c', '--config', default=os.path.join(os.path.dirname(__file__), 'config-newsthreader'), help='config Dir')
    parser.add_argument('--docfreq', type=str,
                        help="Name of file containing docFreq info.. in json format"),
    parser.add_argument('--out', type=str, help="Name of output file")
    args = parser.parse_args()
    main(args)
