#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

import re
import codecs
import requests
import json
from etool import args
from datetime import datetime


def getContent(links):
    for k in links:
        if k:
            try:
                r = requests.get(k)
                return r.text, k
            except Exception, e:
                print str(e)
    return None, None


def main(args):
    if not args.gsr:
        exit(0)
    out = codecs.open(args.out, 'w', encoding='utf-8')
    reader = codecs.open(args.gsr, 'r', encoding='utf-8')
    i = 0
    nullCnt = 0
    for line in reader:
        i += 1
        print "line No: ", i
        try:
            j = json.loads(line.strip(), encoding='utf-8')
        except Exception:
            print "error in parsing json"
            continue
        if not re.match('01.*', j['eventType']):
            continue
        if args.start:
            if j['date'] >= args.start:
                if args.end:
                    if not j['date'] <= args.end:
                        continue
            else:
                continue
        j['content'], j['url'] = getContent([j['derivedFrom']['gssLink'], j['derivedFrom']['otherLinks1'],
                                             j['derivedFrom']['otherLinks2']])
        if j['content'] is None:
            nullCnt += 1
            j['content'] = j['derivedFrom']['description']
            j['url'] = ''
        j['link'] = j['url']
        j['publishedDate'] = j['eventDate']
        j['embersId'] = j['embersId']
        j['news'] = 'True'
        j['crawled'] = j['date']
        j['updated'] = j['date']
        j['trueLocation'] = j['location']
        out.write(json.dumps(j, encoding='utf-8'))
        out.write('\n')
    print "%s articles could not be retrieved", nullCnt
    out.close()


if __name__ == "__main__":
    ap = args.get_parser()
    ap.add_argument('--gsr', type=str, help='file containing gsr warnings')
    ap.add_argument('--out', type=str, default='gsr_%s' % datetime.now().isoformat('T'))
    ap.add_argument('--start', type=str, help="start")
    ap.add_argument('--end', type=str, help="end")
    args = ap.parse_args()
    main(args)
