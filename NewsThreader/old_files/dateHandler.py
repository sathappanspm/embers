#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
    Last Modified:Tue Jan 15 21:19:07 2013
"""

from datetime import datetime
from etool import logs
import re

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"

__processor__ = 'dateHandler'

log = logs.getLogger(__processor__)

dateRE = re.compile('\d\d\d\d-\d\d-\d\d', re.I)


def isodate(date):
    dt = None
    try:
        dtStr = re.findall(dateRE, date)
        if dtStr:
            dt = datetime.strptime(dtStr[0].strip(), '%Y-%m-%d')
        else:
            dt = datetime.strptime(date.strip(), '%Y-%m')
    except ValueError:
        try:
            dt = datetime.strptime(date.strip(), '%Y')
        except ValueError, e:
            log.error('date could not be decoded: %s, %s' % (date, e))
    if dt:
        return dt
    return None
