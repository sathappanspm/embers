#!/usr/bin/env python
#-*- coding:utf-8 -*-
# vim: ts=4 sts=4 sw=4 tw=79 sta et
"""
    *.py: Description of what * does.
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = "find_mostSimilar"
from etool import args, logs, queue
from utils import COUNTRY_REVERSE_CODES, isodate, Similarity, maxHeap, initializeIDF, updateHandler
from dbHandler import columnFamily, dbConnect
from datetime import timedelta

log = logs.getLogger(__processor__)
minCombinedDistanceThreshold = 90
duplicateThreshold = 0.2


class docSearch(object):
    def __init__(self, ndays, CONFIG_DIR, dbname='Embers', buffer_size=1,
                 dbaddr='localhost:9160', distanceMeasure='cosine'):
        self.ndays = ndays
        self.config_dir = CONFIG_DIR
        self.tDelta = timedelta(days=ndays)
        initializeIDF(CONFIG_DIR)
        self.dbCon = dbConnect(dbname, dbaddr)
        self.articleDb = columnFamily(self.dbCon, 'Articles', buffer_size,
                                      ['publishedDate'])
        self.entityDb = columnFamily(self.dbCon, 'Persons', buffer_size)
        self.locationsDb = columnFamily(self.dbCon, 'Locations', buffer_size)
        if distanceMeasure == 'cosine':
            self.calculateDistance = lambda x, y: 1 - Similarity.cosineSimilarity(x, y)
            log.info('Using Cosine Distance')
        elif distanceMeasure == 'soergels':
            self.calculateDistance = Similarity.soergelsDistance
            log.info('using Soergels Distance')
        return

    def getLastN(self, article):
        cCode = COUNTRY_REVERSE_CODES[article['country']]
        endsWith = '_'.join([cCode, (article['publishedDate'] + timedelta(days=1)).strftime('%Y-%m-%d')])  # 'Z' is added in order to include endswith also.
        startsWith = '_'.join([cCode, (article['publishedDate'] - self.tDelta).strftime('%Y-%m-%d')])
        #entKeysIter = self.entityDb.getMany(article["entities"].keys(), startsWith, endsWith)
        #entKeysIter = ()
        #locKeysIter = self.locationsDb.getMany(article["locations"].keys(), startsWith, endsWith)
        #keySet = set()
        #[keySet.update(v.keys()) for k, v in entKeysIter]
        #[keySet.update(v.keys()) for k, v in locKeysIter]
        #return len(keySet), self.articleDb.getMany(list(keySet))
        publisheDateStr = article['publishedDate'].strftime('%Y-%m-%d')
        for k, v in self.articleDb.getRange('%s_%s' % (cCode, publisheDateStr), endsWith):
            yield k, v
        for k, v in self.articleDb.getRange(startsWith, '%s_%s' % (cCode, publisheDateStr)):
            yield k, v
        #return self.articleDb.getByClause('mexico', publishedDate.isoformat('T'))

    def getCombinedDistance(self, doc1, doc2, cosineDistance, threshold):
        """ returns distance: lesser the value more the similarity"""
        locDistance = Similarity.soergelsDistance(doc1['locations'], doc2['locations'])
        entDistance = Similarity.soergelsDistance(doc1['entities'], doc2['entities'])
        total = locDistance + entDistance
        if total > threshold:
            return False
        combinedDistance = 0.5 * (cosineDistance) + 0.35 * locDistance + 0.15 * entDistance
        return combinedDistance

    def insertArticle(self, article, articleKey):
        self.articleDb.insert(articleKey, article)
        #entity_batchObj = {k: {article['embersId']: ''} for k in article['entities']}
        #loc_batchObj = {k: {article['embersId']: ''} for k in article['locations']}
        #self.locationsDb.batch_insert(loc_batchObj)
        #self.entityDb.batch_insert(entity_batchObj)
        return articleKey

    def getTopFive(self, article):
        article['publishedDate'] = isodate(article['publishedDate'])
        mostSimilar = maxHeap(10, 80)
        for key, value in self.getLastN(article):
            distance = self.calculateDistance(article['freqVector'], value['freqVector'])
            if distance < duplicateThreshold:
                if article['protest']:
                    value['protest'] = True
                    self.insertArticle(value, value['embersId'])
                log.info('article %s is a duplicate of url: %s   embersId:%s' % (article['url'],
                                                                                 value['url'], value['embersId']))
                return False
            if self.isDifferentReporting(article, value, distance):
                log.info('Different Reporting %s duplicate' % article['url'])
                return False
            mostSimilar.append(key, int(100 * distance), value)  # only two decimal places precision is used
        topFive = maxHeap(5, minCombinedDistanceThreshold)
        for k, d, v in mostSimilar:
            distance = self.getCombinedDistance(article, v, float(d) / 100, threshold=1.9)
            if distance:
                topFive.append(k, int(distance * 100), v)
        return topFive

    def isDifferentReporting(self, doc1, doc2, distance):
        if doc1['publishedDate'] != doc2['publishedDate']:
            return False
        if distance <= 0.4:
            return True

    def close(self):
        self.articleDb.flush()
        self.entityDb.flush()
        self.locationsDb.flush()
        self.dbCon.close()
        return None


def main(args):
    try:
        reader = queue.open(args.sub, 'r')
        writer = queue.open(args.pub, 'w')
        doc_search = docSearch(args.ndays, args.config, args.dbname, args.buffersize,
                               args.dbaddr, args.simFunc)
        cnt = 0
        updatehandler = updateHandler()
        chnCnt = 0
        for article in reader:
            cnt += 1
            topFive = doc_search.getTopFive(article)
            print cnt
            if not topFive is False:
                mostSimilar = {}
                chainCand = set()
                for key, distance, prop in topFive:
                    mostSimilar[key] = distance
                    chainCand.update(prop['chainIds'])
                article['mostSimilar'] = mostSimilar
                article['chainIds'] = []
                doc_search.insertArticle(article, article['embersId'])
                article['chainCand'] = list(chainCand)
                if article['mostSimilar']:
                    chnCnt += 1
                    print "chainer cnt ", chnCnt
                    writer.write(article)
            if args.updateidf:
                updatehandler.updateDocFreqCounts(article)
                if cnt > 5000:
                    updatehandler.updateIDFTables()
    except KeyboardInterrupt:
        print "closing db"
        doc_search.close()

if __name__ == "__main__":
    import os
    ap = args.get_parser()
    ap.add_argument('--dbaddr', type=str, default='localhost:9160')
    ap.add_argument('--ndays', type=int, default=15)
    ap.add_argument('--dbname', type=str)
    ap.add_argument('--simFunc', type=str, default='cosine')
    ap.add_argument('-c', '--config',
                    default=os.path.join(os.path.dirname(__file__),
                    'config-newsthreader'))
    ap.add_argument('--buffersize', type=int, default=1)
    ap.add_argument('--updateidf', action='store_true', default=False)
    args = ap.parse_args()
    logs.init(args)
    main(args)
