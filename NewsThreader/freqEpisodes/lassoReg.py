#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = ""
import numpy as np
from sklearn.linear_model import Lasso
from sklearn.linear_model import ElasticNet
from sklearn.svm import SVR
import json


def main(args):
    with open(args.fvector) as f:
        fv = json.load(f)
    with open(args.test) as f:
        testVector = json.load(f)
    loc_rows = fv.keys()
    farray = np.array([tuple(fv[k]['features']) for k in loc_rows])
    y = np.array([fv[k]['y'] for k in loc_rows])
    lasso = Lasso(alpha=0.8, max_iter=1000000)
    l = lasso.fit(farray, y)
    ftest = np.array([tuple(testVector[k]['features']) for k in testVector])
    y_test = np.array([testVector[k]['y']for k in testVector])
    print ftest.shape
    print farray.shape
    y_predicted = l.predict(ftest)
    print y_predicted
    print y_test
    print "r^2 on test data : %f" % (1 - np.linalg.norm(y_test - y_predicted) ** 2
                                     / np.linalg.norm(y_test) ** 2)

    print "Elastic Net"
    enet = ElasticNet(alpha=0.4, rho=0.7, max_iter=1000000)
    y_pred_enet = enet.fit(farray, y).predict(ftest)
    print "r^2 on test data : %f" % (1 - np.linalg.norm(y_test - y_pred_enet) ** 2
                                     / np.linalg.norm(y_test) ** 2)

    print y_pred_enet
    print "SVR"
    svr = SVR(kernel='rbf', degree=3)
    y_pred_svr = svr.fit(farray, y).predict(ftest)
    print "r^2 on test data : %f" % (1 - np.linalg.norm(y_test - y_pred_svr) ** 2
                                     / np.linalg.norm(y_test) ** 2)
    print y_pred_svr

if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('--fvector', type=str)
    ap.add_argument('--test', type=str)
    args = ap.parse_args()
    main(args)
