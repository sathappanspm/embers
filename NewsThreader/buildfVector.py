#!/usr/bin/env python
#-*- coding:utf-8 -*-

""" *.py: Description of what * does.  """

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = ""
import json
from utils import isodate


def isprotest(loc, start, end, gsr):
    if loc not in gsr:
        return 0
    return [1 if k > start and k <= end else 0 for k in gsr[loc]][0]


def getNextProtest(loc, start, end, gsr):
    if loc not in gsr:
        return 0
    for dt in gsr[loc]:
        if dt > end:
            return (dt - end).days
    return 0


def main(args):
    with open(args.gsr) as gsrFile:
        gsr = json.load(gsrFile)
        for k in gsr:
            gsr[k] = [isodate(x) for x in gsr[k]]
            gsr[k].sort()
    start = isodate(args.start)
    end = isodate(args.end)
    with open(args.epimap) as epiFile:
        epiMap = json.loads(epiFile.read())
    with open(args.epiloc) as locFile:
        epiLocMap = eval(locFile.read())
    with open(args.sortfile, 'r') as s:
        sortedEpisodes = eval(s.read())
    #sortedEpisodes = [x[0] for x in sorted(epiMap.iteritems(), key=lambda x: x[1])]
    len(sortedEpisodes)
    fVector = {}
    for l in epiLocMap:
        fLoc = {'features': [], 'y': 0}
        fLoc['features'] = [1 if x in epiLocMap[l] else 0 for x in sortedEpisodes]
        if 1 in fLoc['features']:
            #if 0 not in fLoc['features']:
            #    continue
            fLoc['features'].append(isprotest(l, start, end, gsr))
            fLoc['y'] = getNextProtest(l, start, end, gsr)
            if fLoc['y']:
                fVector[l] = fLoc
    #feIndex = -1
    #fIndexDel = []
    #for fe in sortedEpisodes:
    #    feIndex += 1
    #    feBool = [True if fVector[k]['features'][feIndex] else False for k in fVector]

    #    if all(feBool) or not any(feBool):
    #        for k in fVector:
    #            del fVector[k]['features'][feIndex]
    #        fIndexDel.append(feIndex)

    #for i in fIndexDel:
    #    print "del"
    #    del sortedEpisodes[i]
    with open(args.sortfile, 'w') as s:
        s.write(str(sortedEpisodes))

    print len(fVector)
    with open(args.out, 'w') as out:
        out.write(json.dumps(fVector))
    return


if __name__ == "__main__":
    import argparse
    ap = argparse.ArgumentParser()
    ap.add_argument('--epimap', type=str)
    ap.add_argument('--epiloc', type=str)
    ap.add_argument('--gsr', type=str)
    ap.add_argument('--out', type=str)
    ap.add_argument('--start', type=str)
    ap.add_argument('--end', type=str)
    ap.add_argument('--sortfile', type=str)
    args = ap.parse_args()
    main(args)
