#!/usr/bin/env python
#-*- coding:utf-8 -*-

"""
    *.py: Description of what * does.
"""

__author__ = "Sathappan Muthiah"
__email__ = "sathap1@vt.edu"
__version__ = "0.0.1"
__processor__ = ""
from dbHandler import dbConnect, columnFamily
import json


def connect():
    dbCon = dbConnect('Embers_test', '128.173.223.230')
    return columnFamily(dbCon, 'Chains', 1)


def get(chainIds, cf):
    return cf.getMany(chainIds)


def main(args):
    cf = connect()
    episodes = open('/home/vicky/workspace/git/embers/NewsThreader/freqEpisodes/updated_2week.txt')
    locVector = {}
    epiMap = {}
    cnt = 0
    for k in episodes:
        #j = eval(k)
        #epiKey = j['episodeid']
        #if j['freq'] > 0.99:
        #    cnt += 1
        #    continue
        try:
            j = eval(k)
            epiKey = j['parentid']
        except Exception:
            cnt += 1
            continue
        for key, ch in get(j['chainids'], cf):
            for l in ch['locations']:
                if ch['locations'][l] != 0:
                    if l in locVector:
                        locVector[l][epiKey] = 1
                    else:
                        locVector[l] = {epiKey: 1}
        epiMap[epiKey] = j['freq']
    epiMapFile = open('freqEpisodes/episodeMap_2week.txt', 'w')
    epiMapFile.write(json.dumps(epiMap))
    epiMapFile.close()
    print cnt
    with open('freqEpisodes/loc_vector_2weeks.txt', 'w') as out:
        out.write(json.dumps(locVector))
    #print locVector

if __name__ == "__main__":
    import sys
    main(sys.argv)
