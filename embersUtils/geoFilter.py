import json
import sys

def main():
  reader = sys.stdin
  writer = sys.stdout
  for line in reader:
    j = json.loads(line.strip(), encoding='utf-8')
    if j['country'] == 'mexico':
      writer.write(json.dumps(j, encoding='utf-8'))
      writer.write('\n')

if __name__ == "__main__":
  main()
